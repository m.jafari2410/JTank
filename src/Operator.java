import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.RecursiveAction;

/**
 * This class does some operations during the game. Such operations that needs to be checked or done for objects of one class and they
 * must be able to be called now and then in each class. For example method checkBulletPath() needs to be called to see whether the bullet can
 * go on or not. This method maybe called by player or maybe called by enemies. This class has private constructor to avoid instantiate and it's
 * members are static. Also this class has method intersect to check whether to components have intersects or not and is used in move methods of
 * player and also enemies.
 *
 * @author Mohammad Karimi
 * @version 3.0
 * @since 7.12.2018
 */

public class Operator {
    private static ArrayList<Enemy> enemies;
    private static HashMap<Coordinate, Component> inScreenMap;
    private static ArrayList<Rectangle> environmentRectangles = new ArrayList<Rectangle>();
    private static Tank tank;
    private static HashMap<Rectangle, Component> section = new HashMap<Rectangle, Component>();
    public static int levelOfGame = 0;

    private Operator() {
    }

    /**
     * sleep thread
     * @param second seconds for sleep
     */
    public static void sleep(float second) {
        long milliSecond = (long) (second * 1000);
        try {
            Thread.sleep(milliSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks to see whether the bullet can progress more or not. This method uses method intersect to nail this.
     * If the bullet crashes some thing destroyable then the thing will get damage.
     *
     * @param nextPosition next position of bullet.
     * @return true if the bullet can progress more
     */
    public static boolean checkBulletPath(Coordinate nextPosition, Bullet bullet) {
        Component subject = intersects(bullet, nextPosition);
        if (subject == null)
            return true;
        else if (subject instanceof Destroyable) {
            Destroyable destroyable = (Destroyable) subject;
            if (!(destroyable instanceof Enemy) || ((Enemy) destroyable).isEnemyInScreen())
                destroyable.decreaseHealth(bullet.getDamageIntensity());
        }
        return false;
    }

    /**
     * this method show a simple message on screen
     * @param title title of message
     * @param message content of message
     * @param type type of message
     */
    public static void showSimpleMessage(String title, String message, String type) {

        JOptionPane optionPane = null;
        Operator.makeAlert();
        switch (type) {
            case "Error":
                optionPane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
                break;
            case "Info":
                optionPane = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
                break;
        }
        final JDialog dialog = new JDialog();
        dialog.setTitle(title);
        dialog.setModal(true);

        dialog.setContentPane(optionPane);

        dialog.setUndecorated(true);
        dialog.pack();
        dialog.setLocationRelativeTo(null);

        //create timer to dispose of dialog after 3 seconds
        Timer timer = new Timer(3000, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                dialog.dispose();
            }
        });
        timer.setRepeats(false);//the timer should only go off once

        //start timer to close JDialog as dialog modal we must start the timer before its visible
        timer.start();
        dialog.setVisible(true);
    }

    /**
     * This method make a simple alert
     */
    public static void makeAlert() {
        Toolkit.getDefaultToolkit().beep();
    }


    /**
     * This method sets enemies list.
     *
     * @param enemies specified enemies list.
     */
    public static void setEnemies(ArrayList<Enemy> enemies) {
        Operator.enemies = enemies;
    }

    /**
     * This method gets hash map of class map.
     *
     * @param inScreenMap specified hash map.
     */
    public static void setInScreenMap(HashMap<Coordinate, Component> inScreenMap) {
        Operator.inScreenMap = inScreenMap;
    }

    /**
     * This method sets the tank of player to be maintained in class Operator for methods better operations.
     *
     * @param tank specified tank.
     */
    public static void setTank(Tank tank) {
        Operator.tank = tank;
    }

    /**
     * This method makes a hash map from rectangles to component, part of map which is in show screen, and also player tank.
     * And then by help of method intersect from class Rectangle, this method finds out that is there any intersect or not.
     *
     * @param subject      The specified component which we want to see whether it's next position makes any problem or not.
     * @param nextPosition Next position of the specified component.
     * @return true if there is an intersect and false otherwise.
     */
    public static Component intersects(Component subject, Coordinate nextPosition) {
        section.clear();
        environmentRectangles.clear();

        for (Enemy enemy : enemies)
            if (!subject.equals(enemy))
                if (!(subject instanceof BigEnemyBullet) && !(subject instanceof SmallEnemyBullet) && !(subject instanceof MovingEnemyBullet))
                    section.put(new Rectangle(enemy.getPositionInScreen().getX(), enemy.getPositionInScreen().getY(), enemy.getWidth(), enemy.getHeight()), enemy);
        for (Coordinate coordinate : inScreenMap.keySet()) {
            if (inScreenMap.get(coordinate) == null)
                continue;
            section.put(new Rectangle(coordinate.getX(), coordinate.getY(), inScreenMap.get(coordinate).getWidth(), inScreenMap.get(coordinate).getHeight()), inScreenMap.get(coordinate));
            environmentRectangles.add(new Rectangle(coordinate.getX(), coordinate.getY(), inScreenMap.get(coordinate).getWidth(), inScreenMap.get(coordinate).getHeight()));
        }
        if (!subject.equals(tank) && !(subject instanceof CannonBullet || subject instanceof MachineGunBullet))
            section.put(new Rectangle(tank.getXPosRScreen(), tank.getYPosRScreen(), tank.getWidth(), tank.getHeight()), tank);

        Rectangle intendedCase = new Rectangle(nextPosition.getX(), nextPosition.getY(), subject.getWidth(), subject.getHeight());

        for (Rectangle rectangle : section.keySet()) {
            if (rectangle.intersects(intendedCase)) {
                return section.get(rectangle);
            }
        }
        return null;
    }

    /**
     * This method check that two object see together or not
     * @param enemyPos enemy pos
     * @param tankPos tank pos
     * @return true if connect else false
     */
    public static boolean connects(Coordinate enemyPos, Coordinate tankPos) {
        double angle = Math.atan2(tankPos.getY() - enemyPos.getY(), tankPos.getX() - enemyPos.getX());
        float length = (float) Math.sqrt(Math.pow(tankPos.getY() - enemyPos.getY(), 2) + Math.pow(tankPos.getX() - enemyPos.getX(), 2));

        for (float i = 0; i <= length; i++) {
            float x = (float) (enemyPos.getX() + i * Math.cos(angle));
            float y = (float) (enemyPos.getY() + i * Math.sin(angle));

            for (Rectangle rectangle : environmentRectangles) {
                if (rectangle.contains(x, y))
                    return false;
            }
        }
        return true;
    }

    /**
     * this method calculate distance of two object
     * @param coordinate1 coordinate object 1
     * @param coordinate2 coordinate object 2
     * @return distance
     */
    public static double calculateDistance(Coordinate coordinate1, Coordinate coordinate2) {
        return Math.sqrt(Math.pow(coordinate1.getX() - coordinate2.getX(), 2) + Math.pow(coordinate1.getY() - coordinate2.getY(), 2));
    }

    /**
     * This method hurt tank
     * @param damage amount of damage
     */
    public static void hurtPlayer(int damage) {
        tank.decreaseHealth(damage);
    }
}