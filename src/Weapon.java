/**
 * This class models weapon of a tank.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 7.8.2018
 */

import java.awt.*;

public abstract class Weapon {


    private int xPos;
    private int yPos;
    private int numberOfBullets;
    private static int firstNumberOfBullets;
    private Bullet bullet;
    //image of the weapon
    private Image imageOfWeapon;
    private Image secImageOfWeapon;
    //level of weapon
    private int level = 1;
    //position of weapon relative to center of the tank
    private Coordinate positionRCenterOfTank;
    //position of anchor which weapon is rotated around that
    private Coordinate anchor;


    /**
     * @return x position of weapon
     */
    public int getXPos() {
        return xPos;
    }

    /**
     * @return y position of weapon
     */
    public int getYPos() {
        return yPos;
    }

    /**
     * @return number of bullets
     */
    public int getNumberOfBullets() {
        return numberOfBullets;
    }

    /**
     * This method sets the number of bullets.
     *
     * @param numberOfBullets number of bullets
     */
    public void setNumberOfBullets(int numberOfBullets) {
        this.numberOfBullets = numberOfBullets;
    }

    /**
     * This method set horizontal position of the weapon
     *
     * @param xPos horizontal position of the weapon
     */
    public void setXPos(int xPos) {
        this.xPos = xPos;
    }

    /**
     * This method set vertical position of the weapon
     *
     * @param yPos vertical position of the weapon
     */
    public void setYPos(int yPos) {
        this.yPos = yPos;
    }

    /**
     * This method increases number of bullets.
     *
     * @param i number of increase in bullets
     */
    public void increaseBullets(int i) {
        numberOfBullets += i;
    }

    /**
     * This method decreases number of bullets.
     */
    public void decreaseBullets() {
        numberOfBullets--;
    }


    /**
     * Sets image of the weapon
     *
     * @param imageOfWeapon image of the weapon
     */
    public void setImageOfWeapon(Image imageOfWeapon) {
        this.imageOfWeapon = imageOfWeapon;
    }

    /**
     * Sets image of the weapon
     *
     * @param secImageOfWeapon image of the weapon
     */
    public void setSecImageOfWeapon(Image secImageOfWeapon) {
        this.secImageOfWeapon = secImageOfWeapon;
    }

    /**
     * @return image of the weapon
     */
    public Image getImageOfWeapon() {
        if (level == 1)
            return imageOfWeapon;
        else
            return secImageOfWeapon;
    }

    /**
     * This method sets bullet of the tank
     */
    public void setBullet(Bullet bullet) {
        this.bullet = bullet;
    }

    /**
     * This method gets bullet of the tank
     *
     * @return bullet type
     */
    public Bullet getBullet() {
        return bullet;
    }

    /**
     * This method increases level of the weapon, upgrades bullets and change image of the weapon.
     */
    public void increaseLevel() {
        if (level == 1) {
            level++;
            getBullet().upgrade();
            imageOfWeapon = secImageOfWeapon;
        }
    }

    /**
     * @return level of the weapon.
     */
    public int getLevel() {
        return level;
    }

    /**
     * This method makes bullets in according to bullet type of the weapon.
     *
     * @param xPos    x position of tank
     * @param yPos    y position of tank
     * @param xTarget x target
     * @param yTarget y target
     * @return bullet
     * @throws CloneNotSupportedException if clone was now supported.
     */
    public Bullet makeBullet(int xPos, int yPos, int xTarget, int yTarget) throws CloneNotSupportedException {
        return getBullet().getInstance(xPos, yPos, xTarget, yTarget);

    }

    /**
     * This method sets initially position of weapon relative to center of tank.
     *
     * @param positionRCenterOfTank initially position of weapon relative to center of tank.
     */
    public void setPositionRCenterOfTank(Coordinate positionRCenterOfTank) {
        this.positionRCenterOfTank = positionRCenterOfTank;
    }

    /**
     * This method returns current position of the weapon relative to screen not map.
     *
     * @param angleOfTank The angle which tank has been rotated.
     * @return coordination of which weapon is drawn.
     */
    public Coordinate getPosition(double angleOfTank, Coordinate centerOfTank) {

        int initXc = positionRCenterOfTank.getX();
        int initYc = positionRCenterOfTank.getY();

        int centerX = centerOfTank.getX();
        int centerY = centerOfTank.getY();

        positionRCenterOfTank.setX((int) (initXc * Math.cos(angleOfTank) - initYc * Math.sin(angleOfTank)));
        positionRCenterOfTank.setY((int) (initXc * Math.sin(angleOfTank) + initYc * Math.cos(angleOfTank)));

        return new Coordinate(positionRCenterOfTank.getX() + centerX, positionRCenterOfTank.getY() + centerY);
    }

    /**
     * This method sets the anchor which the weapon is rotated around it.
     *
     * @param anchor The anchor which the weapon is rotated around it.
     */
    public void setAnchor(Coordinate anchor) {
        this.anchor = anchor;
    }

    /**
     * This method gets the anchor of the weapon.
     *
     * @return anchor of the weapon.
     */
    public Coordinate getAnchor() {
        return anchor;
    }

    public void setFirstNumberOfBullets(int firstNumberOfBullets) {
        this.firstNumberOfBullets = firstNumberOfBullets;
    }

    public int getFirstNumberOfBullets() {

        return firstNumberOfBullets;
    }
}
