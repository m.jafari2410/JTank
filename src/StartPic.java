import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
/**
 * This class show a pic when game execute
 * @author Mohamma Karimi
 * @version 1.0
 * @since 8.7.2018
 */
public class StartPic extends JDialog {

    private JFrame pic;
    private Audio audio;
    private static StartPic startPic = null;

    /**
     * Constructor of class
     */
    private StartPic(){
    }

    public void init(){
        pic = new JFrame();
        GraphicOperator.setFrameIcon(pic,"data/icon/gameIcon.jpg");
        pic.setSize(600,400);
        pic.setUndecorated(true);
        pic.setResizable(false);
        pic.setLocationRelativeTo(null);
        Drawer.drawImage(pic,"data/image/startPic.jpg",-30,-30);
        pic.setVisible(true);

        audio = new Audio("data/sound/start.wav");
        audio.start(0);

        Drawer.typeText(pic,"Version 1.0.0",10,390,new Font(pic.getToolkit().getFontList()[2],Font.ITALIC + Font.BOLD,20),Color.RED);
        pic.setVisible(true);
        String s = "";
        char[] strings = {'B','a','t','t','l','e',' ','O','f',' ','T','a','n','k','s'};
        int i = 0;
        while (s.length() < 15 ){
            s += strings[i];
            i++;
            Drawer.typeText(pic,s,120,50,new Font(pic.getToolkit().getFontList()[2],Font.ITALIC,50),Color.BLACK);
            pic.setVisible(true);
            Operator.sleep(0.3f);
        }
        Operator.sleep(3);
        audio.stop();
        GraphicOperator.closeFrameAndAudio(audio,pic);
    }

    /**
     * get an instance of class
     * @return instance
     */
    public static StartPic getInstance(){
        if (startPic == null)
            startPic = new StartPic();
        return startPic;
    }

}
