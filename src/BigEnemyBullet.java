import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class BigEnemyBullet extends Bullet {
    public BigEnemyBullet(){
        try {
            setImage(ImageIO.read(new File("data/image/Enemy2Bullet.png")));
            setExplosionImage(ImageIO.read(new File("data/image/HeavyFire.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        switch (Operator.levelOfGame){
            case 0:
                setDamageIntensity(25);
                break;
            case 1:
                setDamageIntensity(40);
                break;
            case 2:
                setDamageIntensity(100);
                break;
        }

        setWidth(20);
        setHeight(10);
    }
}
