/**
 * This class show a pic when game execute
 * @author Mohamma Karimi
 * @version 1.0
 * @since 9.7.2018
 */

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * This class do graphic operations
 * @author Mohammad Karimi
 * @version 2.1
 * @since 8.7.2018
 */
public class GraphicOperator {

    private GraphicOperator(){

    }

    /**
     * set of buttona
     * @param buttons buttons
     * @param enteredName name buttons when mouse in
     * @param exitedName name buttons when mouse exit
     * @param address address of icons
     * @param x for set pos of icons
     */
    public static void setButtons(ArrayList<JButton> buttons, ArrayList<String> enteredName, ArrayList<String> exitedName, String address, int x){
        for (int i = 0 ; i < buttons.size() ; i++){
            setIconButton(buttons.get(i),  address + enteredName.get(i) + ".png");
            int finalI = i;
            ArrayList<String> finalEnteredName = enteredName;
            ArrayList<String> finalExitedName = exitedName;
            String finalAddress = address;
            buttons.get(i).addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    setIconButton(buttons.get(finalI), finalAddress + finalExitedName.get(finalI) + ".png");
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    setIconButton(buttons.get(finalI),finalAddress + finalEnteredName.get(finalI) + ".png");
                }
            });
            buttons.get(i).setBounds(1280 / 2 + 32,720 / 2 - 270 + (i * x),100,100);
            buttons.get(i).setContentAreaFilled(false);
            buttons.get(i).setBorder(null);
            buttons.get(i).setOpaque(false);
            buttons.get(i).setToolTipText(enteredName.get(i));
        }
    }

    /**
     * This method set an icon for a button
     * @param button button
     * @param address address of icon
     */
    public static void setIconButton(JButton button,String address){
        button.setIcon(new ImageIcon(address));
    }

    /**
     * This method close a frame and stop it's audio
     * @param audio audio of frame
     * @param frame frame for close
     */
    public static void closeFrameAndAudio(Audio audio, JFrame frame){
        audio.clip.close();
        frame.dispose();
    }

    /**
     * This method close a frame (dispose it)
     * @param frame frame for close
     */
    public static void closeFrame(JFrame frame){
        frame.dispose();
    }

    /**
     * this method set focus on a frame
     * @param frame frame
     */
    public static void setFocus(JFrame frame){
        frame.setFocusable(true);
        frame.requestFocusInWindow();
    }

    /**
     * This method set frame icon
     * @param frame frame
     * @param address address of icon
     */
    public static void setFrameIcon(JFrame frame,String address){
        try {
            frame.setIconImage(ImageIO.read(new File(address)));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
    }

    /**
     * this method get list of look and feels
     * @return list of look and feels
     */
    public static UIManager.LookAndFeelInfo[] getLookAndFeelsList(){
        return UIManager.getInstalledLookAndFeels();
    }

    /**
     * This method set a look and feel
     * @param lookAndFeelInfo look and feel for set
     */
    public static void setLookAndFeel(UIManager.LookAndFeelInfo lookAndFeelInfo){
        try {
            UIManager.setLookAndFeel(lookAndFeelInfo.getClassName());
        } catch (Exception e) {
            Errors.handler("lookAndFeelError");
        }
    }


}
