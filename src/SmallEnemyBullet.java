import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class SmallEnemyBullet extends Bullet {

    public SmallEnemyBullet(){
        try {
            setImage(ImageIO.read(new File("data/image/EnemyBullet1.png")));
            setExplosionImage(ImageIO.read(new File("data/image/HeavyFire.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        switch (Operator.levelOfGame){
            case 0:
                setDamageIntensity(10);
                break;
            case 1:
                setDamageIntensity(20);
                break;
            case 2:
                setDamageIntensity(40);
                break;
        }

        setWidth(23);
        setHeight(9);
    }
}
