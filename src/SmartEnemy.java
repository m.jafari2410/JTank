/**
 * This class models an smart enemy which has artificial intelligence to find the player then sometimes move
 * and sometimes shoot toward that. Smart enemies list is comprised of list of fixed enemies and moving enemies.
 * This class has a method to shoot and also has a method to see if the player is visible by the enemy. If player
 * was visible then the enemy shoots.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 7.12.2018
 */

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SmartEnemy extends Enemy {
    //bullet type of enemy
    private Bullet bullet;
    private Image imageOfGun;
    //shoots of enemy
    private ArrayList<Bullet> shoots;
    private int shootInterruptNumber;
    private int limitNumberOfRepetitiveShoots;

    /**
     * This is constructor and calls super constructor.
     *
     * @param positionInMap is position of enemy in map.
     */
    public SmartEnemy(Coordinate positionInMap) {
        super(positionInMap);
        shoots = new ArrayList<Bullet>();
        try {
            setDeathImage(ImageIO.read(new File("data/image/explosion.png")));
            setAfterDeathImage(ImageIO.read(new File("data/image/destroyed.png")));
            setDestroyAudio(new Audio("data/sound/enemyTankDestroy.wav"));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setShowNumberOfDeathImage(30);
        setShowNumberOfAfterDeathImage(200);
    }

    /**
     * This method updates state of the smart enemy and if player tank is seen by the enemy then the enemy does shooting toward player tank.
     *
     * @param playerCoordinate position of player tank.
     */
    @Override
    public void updateState(Coordinate playerCoordinate) {
        setPlayerCoordinate(playerCoordinate);
        setDrawPermit(true);
        if (isPlayerVisible()) {
            shoot();
        }
    }

    /**
     * This method does shooting. In fixed enemy shooting is done when the player is seen by enemy but
     * in moving enemies shooting is done after some moving that cause player to be seen better.
     */
    public void shoot() {
        try {
            if (shootInterruptNumber > limitNumberOfRepetitiveShoots) {
                shoots.add(bullet.getInstance((int) (getPositionInScreen().getX() + getWidth() / 2 + Math.cos(getAngle()) * getWidth() / 2), (int) (getPositionInScreen().getY() + getHeight() / 2 + Math.sin(getAngle()) * getHeight() / 2), getPlayerCoordinate().getX() + 50, getPlayerCoordinate().getY() + 50));
                shootInterruptNumber = 0;
            }
            shootInterruptNumber++;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }


    /**
     * This method sets bullet type of enemy tank.
     *
     * @param bullet Bullet type of enemy tank.
     */
    public void setBullet(Bullet bullet) {
        this.bullet = bullet;
    }

    /**
     * This method gets bullet type of enemy tank.
     *
     * @return Bullet type of enemy tank.
     */
    public Bullet getBullet() {
        return bullet;
    }

    /**
     * This method sets image of tank gun.
     *
     * @param imageOfGun image of tank gun.
     */
    public void setImageOfGun(Image imageOfGun) {
        this.imageOfGun = imageOfGun;
    }

    /**
     * This method gets image of tank gun.
     *
     * @return image of tank gun.
     */
    public Image getImageOfGun() {
        return imageOfGun;
    }

    /**
     * This method gets list of shoots.
     *
     * @return list of shoots.
     */
    public ArrayList<Bullet> getShoots() {
        return shoots;
    }

    public void setLimitNumberOfRepetitiveShoots(int limitNumberOfRepetitiveShoots) {
        this.limitNumberOfRepetitiveShoots = limitNumberOfRepetitiveShoots;
    }
}
