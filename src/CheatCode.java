import java.util.ArrayList;

/**
 * This class store information of cheatCodes
 * @author Mohamma Karimi
 * @version 1.0
 * @since 14.7.2018
 */
public class CheatCode {

    /**
     * list of cheat Codes
     */
    private static ArrayList<String> cheatCodes = new ArrayList<>();
    private CheatCode(){

    }
    /**
     * Specifies whether a string is a cheat code or not
     * @param cheatCode input sting for test
     * @return return input string if is cheat code else return null
     */
    public static String isCheatCode(String cheatCode) {
        for (String s : cheatCodes) {
            for (int i = 0; i < cheatCode.length(); i++) {
                for (int j = i + 1; j <= cheatCode.length(); j++) {
                    if (s.equals(cheatCode.substring(i, j))){
                        return s;
                    }
                }

            }
        }
        return null;
    }

    /**
     * this method apply cheat code
     * @param tank player tank
     * @param cheatCode cheat code
     */
    public static void applyCode(Tank tank,String cheatCode){
        if (cheatCode.equalsIgnoreCase("uhealthy")){
            tank.setHealth(Integer.MAX_VALUE);
        } else if (cheatCode.equalsIgnoreCase("fhealthy")){
            tank.setHealth(tank.getFirstHealth());
        } else if (cheatCode.equalsIgnoreCase("ubullet")){
            tank.getActiveWeapon().setNumberOfBullets(Integer.MAX_VALUE);
        } else if (cheatCode.equalsIgnoreCase("fbullet")){
            tank.getActiveWeapon().setNumberOfBullets(tank.getActiveWeapon().getFirstNumberOfBullets());
        } else if (cheatCode.equalsIgnoreCase("uweapon")){
            tank.upgradeWeapon();
        }
        KeyHandler.setCheatCode("");
    }

    /**
     * add a cheat code to list
     * @param string
     */
    public static void addCheatCode(String string){
        cheatCodes.add(string);
    }

}
