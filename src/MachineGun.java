import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * This class models a machine gun and extends from class Weapon.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 7.8.2018
 */
public class MachineGun extends Weapon {

    /**
     * This is constructor and initials fields of the class.
     */
    public MachineGun() {
        setBullet(new MachineGunBullet());
        try {
            setImageOfWeapon(ImageIO.read(new File("data/image/MachineGun1.png")));
            setSecImageOfWeapon(ImageIO.read(new File("data/image/MachineGun2.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        setFirstNumberOfBullets(300);
        setNumberOfBullets(getFirstNumberOfBullets());
        setPositionRCenterOfTank(new Coordinate(-27,-50));
        setAnchor(new Coordinate(30,47));
    }
}
