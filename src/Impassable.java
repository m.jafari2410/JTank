/**
 * This class models an impassable component and extends class Component.
 *
 * @author Mohamma Jafari
 * @version 1.0
 * @since 7.9.2018
 */

public class Impassable extends Component {
}
