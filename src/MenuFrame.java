import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Menu frame of game
 * @author Mohamma Karimi
 * @version 1.0
 * @since 8.7.2018
 */

public class MenuFrame {

    private JFrame menuFrame;
    private boolean isOpen = true;
    private int amountOfVolume = 100;
    private boolean isMute = false;
    private static ArrayList<JButton> buttons = new ArrayList<JButton>();
    private static ArrayList<JButton> volumeButtons = new ArrayList<JButton>();
    private JLabel background;
    private Audio audio;
    private static MenuFrame menu = null;
    private static boolean isMainMenu = true;

    private MenuFrame(){
    }


    /**
     * get an instance of class
     * @return instance of class
     */
    public static MenuFrame getInstance(){
        if (menu == null)
            menu = new MenuFrame();
        return menu;
    }

    /**
     * init menu
     */
    public void init(){

        menuFrame = new JFrame("Battle of tanks");
        GraphicOperator.setFrameIcon(menuFrame,"data/icon/gameIcon.jpg");
        menuFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        menuFrame.setUndecorated(true);
        menuFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        menuFrame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
                    isOpen = false;
                    System.exit(0);
                }
            }

        });


        menuFrame.setLayout(new BorderLayout());
        background = new JLabel(new ImageIcon("data/image/menuBackground.jpg"));

        menuFrame.add(background);
        create();

        makeVolumeButtons();
        showTime();
        GraphicOperator.setFocus(menuFrame);
        menuFrame.setVisible(true);

        audio = new Audio("data/sound/menu.wav");
        audio.start(-1);

    }

    /**
     * menu buttons list
     * @return menu buttons list
     */
    public ArrayList<JButton> menuButtons(){
        JButton recentGame = new JButton();
        JButton onePlayerGame = new JButton();
        JButton twoPlayerGame = new JButton();
        JButton about = new JButton();
        JButton designMap = new JButton();
        JButton exit = new JButton();
        buttons.add(recentGame);
        buttons.add(onePlayerGame);
        onePlayerGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isMainMenu = false;
                update();
                GraphicOperator.setFocus(menuFrame);
            }
        });
        buttons.add(twoPlayerGame);
        buttons.add(about);
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AboutFrame();
            }
        });
        buttons.add(designMap);
        designMap.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new DesignMap();
            }
        });
        buttons.add(exit);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        GraphicOperator.setButtons(buttons,menuButtonEnteredName(),menuButtonExitedName(),"data/icon/menu/",90);
        return buttons;
    }


    /**
     * menu buttons name
     * @return menu buttons name
     */
    public ArrayList<String> menuButtonEnteredName(){
        ArrayList<String> enteredName = new ArrayList<>();
        enteredName.add("Recent Game");
        enteredName.add("One-Player Game");
        enteredName.add("Two-Player Game");
        enteredName.add("About And Help");
        enteredName.add("Design Map");
        enteredName.add("Exit");
        return enteredName;

    }

    /**
     * menu buttons name when mouse exit
     * @return menu buttons when mouse exit
     */
    public ArrayList<String> menuButtonExitedName(){
        ArrayList<String> exitedName = new ArrayList<>();
        exitedName.add("Recent Game2");
        exitedName.add("One-Player Game2");
        exitedName.add("Two-Player Game2");
        exitedName.add("About And Help2");
        exitedName.add("Design Map2");
        exitedName.add("Exit2");
        return exitedName;
    }

    /**
     * level menu buttons list
     * @return level menu buttons list
     */
    public ArrayList<JButton> levelMenuButtons(){
        JButton easy = new JButton();
        JButton normal = new JButton();
        JButton hard = new JButton();
        class LevelHandler implements ActionListener{
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == easy){
                    Operator.levelOfGame = 0;
                } else if (e.getSource() == normal){
                    Operator.levelOfGame = 1;
                } else if (e.getSource() == hard){
                    Operator.levelOfGame = 2;
                }
                ThreadPool.init();
                EventQueue.invokeLater(new Runnable() {
                    Boolean isFinish = false;
                    @Override
                    public void run() {
                        GameFrame frame = new GameFrame("Battle of tanks");
                        frame.setLocationRelativeTo(null); // put frame at center of screen
                        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                        frame.setVisible(true);
                        frame.initBufferStrategy();
                        // Create and execute the game-loop
                        GameLoop game = new GameLoop(frame);
                        ThreadPool.execute(game);
                        // and the game starts ...
                    }
                });
                isMainMenu = true;
                buttons.clear();
                GraphicOperator.closeFrameAndAudio(audio,menuFrame);
            }
        }
        JButton backToMainMenu = new JButton();
        buttons.add(easy);
        easy.addActionListener(new LevelHandler());
        buttons.add(normal);
        normal.addActionListener(new LevelHandler());
        buttons.add(hard);
        hard.addActionListener(new LevelHandler());
        buttons.add(backToMainMenu);
        buttons.get(3).addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                isMainMenu = true;
                update();
                GraphicOperator.setFocus(menuFrame);
            }
        });
        GraphicOperator.setButtons(buttons,levelMenuButtonEnteredName(),levelMenuButtonExitedName(),"data/icon/levelMenu/",100);
        return buttons;
    }

    /**
     * level menu buttons name
     * @return level menu buttons name
     */
    public ArrayList<String> levelMenuButtonEnteredName(){
        ArrayList<String> enteredName = new ArrayList<>();
        enteredName.add("Easy");
        enteredName.add("Normal");
        enteredName.add("Hard");
        enteredName.add("Back To Main Menu");
        return enteredName;
    }

    /**
     * level menu buttons name when mouse exit
     * @return levve menu buttons when mouse exit
     */
    public ArrayList<String> levelMenuButtonExitedName(){
        ArrayList exitedName = new ArrayList();
        exitedName.add("Easy2");
        exitedName.add("Normal2");
        exitedName.add("Hard2");
        exitedName.add("Back To Main Menu2");
        return exitedName;
    }
    private void create(){
        buttons.clear();
        if (isMainMenu){
            buttons = menuButtons();
            addButtons(buttons);
        }
        else {
            buttons = levelMenuButtons();
            addButtons(buttons);
        }
    }


    /**
     * add a button to input list
     * @param buttons list of buttons
     */
    private void addButtons(ArrayList<JButton> buttons){
        for (int i = 0 ; i < buttons.size() ; i++){
            background.add(buttons.get(i));
        }
    }

    /**
     * remove buttons from input list
     * @param buttons buttons list
     */
    private void removeButtons(ArrayList<JButton> buttons){
        int i = 0;
        for ( i = 0 ; i < buttons.size() ; i++){
            background.remove(buttons.get(i));
            background.revalidate();
            background.repaint();
        }
        buttons.clear();
    }

    /**
     * update list of buttons
     */
    private void update(){
        removeButtons(buttons);
        create();
        addButtons(buttons);
    }

    /**
     * make volume buttons
     */
    private void makeVolumeButtons(){
        String address = "data/icon/menu/volume/";
        JButton increase = new JButton();
        GraphicOperator.setIconButton(increase,address + "increase.png");
        increase.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(amountOfVolume < 100){
                    isMute = false;
                    audio.setVolume(audio.getVolume() + 0.05f);
                    amountOfVolume += 5;
                    updateVolumeButton();
                }
                GraphicOperator.setFocus(menuFrame);
            }
        });
        increase.setToolTipText("Increase the volume");
        JButton decrease = new JButton();
        GraphicOperator.setIconButton(decrease,address + "decrease.png");
        decrease.setToolTipText("decrease the volume");
        decrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(amountOfVolume != 0){
                    isMute = false;
                    if(audio.getVolume() - 0.05 < 0.05)
                        audio.setVolume(0);
                    else
                        audio.setVolume((float) (audio.getVolume() - 0.05) );
                    amountOfVolume -= 5;
                    updateVolumeButton();
                }
                GraphicOperator.setFocus(menuFrame);
            }
        });
        JButton mute = new JButton();
        GraphicOperator.setIconButton(mute,address + "mute2.png");
        mute.setToolTipText("Mute");
        mute.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!isMute){
                    audio.mute();
                    isMute = true;
                    updateVolumeButton();
                } else {
                    isMute = false;
                    audio.setVolume(amountOfVolume/100);
                    updateVolumeButton();
                }
                GraphicOperator.setFocus(menuFrame);
            }
        });
        JButton volume = new JButton();

        volumeButtons.add(volume);
        volumeButtons.add(increase);
        volumeButtons.add(decrease);
        volumeButtons.add(mute);
        volumeButtons.get(1).setBounds(140,720 - 30 ,70,70);
        volumeButtons.get(2).setBounds(10,720 - 30 ,70,70);
        volumeButtons.get(3).setBounds(75,720 - 90 ,70,70);

        updateVolumeButton();

        addButtons(volumeButtons);

        volumeButtons.get(0).setBounds(70,720 - 30 ,70,70);

        for (int i = 0 ; i < 4 ; i++){
            volumeButtons.get(i).setContentAreaFilled(false);
            volumeButtons.get(i).setBorder(null);
            volumeButtons.get(i).setOpaque(false);
        }


    }

    /**
     * update volume buttons
     */
    private void updateVolumeButton(){
        String address = "data/icon/menu/volume/";
        if(isMute)
            GraphicOperator.setIconButton(volumeButtons.get(0),address + "mute.png");
        else {
            if(amountOfVolume > 66 && amountOfVolume <= 100)
                GraphicOperator. setIconButton(volumeButtons.get(0),address + "speaker1.png");
            else if(amountOfVolume <= 66 && amountOfVolume > 33)
                GraphicOperator. setIconButton(volumeButtons.get(0),address + "speaker2.png");
            else if(amountOfVolume <= 33 && amountOfVolume >= 1)
                GraphicOperator. setIconButton(volumeButtons.get(0),address + "speaker3.png");
            else
                GraphicOperator. setIconButton(volumeButtons.get(0),address + "mute.png");
        }

        String toolTip = "";
        if (isMute || amountOfVolume == 0){
            toolTip = "Muted";
        } else {
            toolTip = "" + amountOfVolume + " %";
        }
        volumeButtons.get(0).setToolTipText(toolTip);
    }

    /**
     * show time and date i screen
     */
    public void showTime(){
         class DataAndTime extends SwingWorker {
            JLabel time = new JLabel();
            JLabel background;
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

            public DataAndTime(JLabel background) {
                this.background = background;
            }

            LocalDateTime now = LocalDateTime.now();
            @Override
            protected Integer doInBackground() throws Exception {
                while (isOpen){
                    now = LocalDateTime.now();
                    publish(dtf.format(now));
                }
                return null;
            }

            @Override
            protected void process(List chunks) {
                super.process(chunks);
                background.add(time);
                time.setForeground(Color.white);
                time.setFont(new Font(time.getToolkit().getFontList()[2],Font.BOLD,20));
                time.setBounds(1280 - 155,720 - 30,300,100);
                time.setText((String) chunks.get(0));
                background.revalidate();
                background.repaint();
            }
        }
        new DataAndTime(background).execute();
    }


}
