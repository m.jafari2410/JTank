

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * This class describes enemy as overall. The enemy extends class Destroyable so it is a component. Each enemy is activated just when
 * the enemy is in show screen not total map. And also the enemy is deactivated when it is going out of the screen.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 7.13.2018
 */
public abstract class Enemy extends Destroyable {
    //position of enemy in total map
    private Coordinate positionInMap;
    //position of enemy in screen
    private Coordinate positionInScreen;
    //the angle of line from enemy to player
    private double angle;
    private boolean drawPermit = false;
    private Coordinate playerCoordinate;
    private Coordinate anchor;


    public Enemy(Coordinate positionInMap) {
        this.positionInMap = positionInMap;
        positionInScreen = new Coordinate();
    }

    /**
     * This method updates position of the enemy relative to screen and returns true if the enemy is in screen now.
     * This method is called in class GameLoop and if that was true then method updateState is called.
     *
     * @return true if the enemy is in screen now.
     */
    public boolean isEnemyInScreen() {
        positionInScreen.setX(positionInMap.getX() - GameFrame.screenPosition.getX());
        positionInScreen.setY(positionInMap.getY() - GameFrame.screenPosition.getY());

        if (positionInScreen.getX() >= -100 && positionInScreen.getX() <= GameFrame.GAME_WIDTH)
            if (positionInScreen.getY() >= -100 && positionInScreen.getY() <= GameFrame.GAME_HEIGHT) {
                return true;
            }
        return false;
    }

    /**
     * This method checks to see if the player is visible.
     *
     * @return true if the player is visible for enemy.
     */
    public boolean isPlayerVisible() {
        if (Operator.connects(this.positionInScreen, playerCoordinate))
            return true;
        return false;
    }

    /**
     * This method updates state of the enemy.
     *
     * @param playerCoordinate position of player tank.
     */
    public abstract void updateState(Coordinate playerCoordinate);

    /**
     * This method calculates angle line from enemy to player tank.
     *
     * @return angle of line from enemy to player tank.
     */
    public double getAngle() {
        angle = Math.atan2((double) (playerCoordinate.getY() - positionInScreen.getY()), (double) (playerCoordinate.getX() - positionInScreen.getX()));
        return angle;
    }

    /**
     * This method sets position of enemy in map.
     *
     * @param positionInMap position of enemy in map.
     */
    public void setPositionInMap(Coordinate positionInMap) {
        this.positionInMap = positionInMap;
    }

    /**
     * This method gets position of enemy in map.
     *
     * @return position of enemy in map
     */
    public Coordinate getPositionInMap() {
        return positionInMap;
    }

    /**
     * This method gets position of enemy in screen.
     *
     * @return position of enemy in screen.
     */
    public Coordinate getPositionInScreen() {
        return positionInScreen;
    }

    /**
     * This method says whether draw of enemy has permission or not.
     *
     * @return true if draw of enemy has permission.
     */
    public boolean isDrawable() {
        return drawPermit;
    }

    /**
     * This method sets permission of draw.
     *
     * @param drawPermit true if draw of enemy is allowed.
     */
    public void setDrawPermit(boolean drawPermit) {
        this.drawPermit = drawPermit;
    }

    public void setPlayerCoordinate(Coordinate playerCoordinate) {
        this.playerCoordinate = playerCoordinate;
    }

    public Coordinate getPlayerCoordinate() {
        return playerCoordinate;
    }


    public AffineTransform getAffineTransform() {
        AffineTransform affineTransform = AffineTransform.getTranslateInstance(positionInScreen.getX() + 30,positionInScreen.getY() + 30);
        affineTransform.rotate(getAngle(),anchor.getX(), anchor.getY());
        return affineTransform;
    }

    public void setAnchor(Coordinate anchor) {
        this.anchor = anchor;
    }

    public Coordinate getAnchor() {
        return anchor;
    }
}
