import jdk.nashorn.internal.scripts.JO;
import sun.audio.*;

import javax.sound.sampled.*;
import javax.swing.*;
import java.io.*;

/**
 * This class handle sounds in game
 * @author Mohamma Karimi
 * @version 1.0
 * @since 8.7.2018
 */

public class Audio {

    AudioInputStream audioInputStream;
    String nameOfAudioFile;
    Clip clip;

    /**
     * Make audio file
     * @param nameOfAudioFile name of audio file
     */
    public Audio(String nameOfAudioFile){
        this.nameOfAudioFile = nameOfAudioFile;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(new File(nameOfAudioFile));
        } catch (Exception e) {
            Errors.handler("loadingError");
        }
        try {
            clip = AudioSystem.getClip();
        } catch (Exception e) {
            Errors.handler("loadingError");
        }
        try {
            clip.open(audioInputStream);
        } catch (Exception e) {
            Errors.handler("loadingError");
        }
    }

    /**
     * play audio
     * @param repeatNumber number of repeat audio
     */
    public void start(int repeatNumber){
        clip.start();
        switch (repeatNumber){
            case -1:
                clip.loop(Clip.LOOP_CONTINUOUSLY);
                break;
            default:
                clip.loop(repeatNumber);
                break;
        }
    }

    /**
     * stop audio
     */
    public void stop(){
        clip.stop();
    }

    /**
     * reset audio
     */
    public void reset(){
        audioDataStream.reset();
        continuousAudioDataStream.reset();
    }

    /**
     * get volume
     * @return volume
     */
    public float getVolume() {
        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        return (float) Math.pow(10f, gainControl.getValue() / 20f);
    }

    /**
     * set volume
     * @param volume volume
     */
    public void setVolume(float volume) {
        if (volume < 0f || volume > 1f){
        } else {
            FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            gainControl.setValue(20f * (float) Math.log10(volume));
        }
    }

    /**
     * mute audio
     */
    public void mute(){
        setVolume(0);
    }

    /**
     * start audio from first
     */
    public void startFromFirst(){
        if (clip.isOpen()){
            clip.start();
        } else {
            try {
                audioInputStream = AudioSystem.getAudioInputStream(new File(nameOfAudioFile));
            } catch (Exception e) {
                Errors.handler("loadingError");
            }
            try {
                clip = AudioSystem.getClip();
            } catch (Exception e) {
                Errors.handler("loadingError");
            }
            try {
                clip.open(audioInputStream);
            } catch (Exception e) {
                Errors.handler("loadingError");
            }
            clip.start();
        }
    }
    //*********************************************************
    FileInputStream fis = null;
    AudioStream as = null; // header plus audio data
    AudioData ad = null; // audio data only, no header
    AudioPlayer audioPlayer = AudioPlayer.player;
    AudioDataStream audioDataStream;
    ContinuousAudioDataStream continuousAudioDataStream;
    // Applet role
    public void init() {
        // Get sound from file stream.

        try {
            fis = new FileInputStream( new File(nameOfAudioFile) );
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            as = new AudioStream( fis );
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            ad = as.getData();
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        audioDataStream = new AudioDataStream( ad );
        continuousAudioDataStream = new ContinuousAudioDataStream( ad );
    }

    /**
     * play an effect
     */
    public void startEffect(){
        init();
        audioPlayer.start(audioDataStream);
    }

    /**
     * stop an effect
     */
    public void stopEffect(){
        audioPlayer.stop(audioDataStream);
        audioPlayer.stop(continuousAudioDataStream);
    }

}
