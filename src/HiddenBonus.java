public class HiddenBonus extends Bonus {
    private Destroyable destroyable;
    private boolean drawPermit = false;


    public HiddenBonus(Coordinate place, Destroyable destroyable) {
        super(place);
        this.destroyable = destroyable;
    }

    @Override
    public void update() {
        if (destroyable.hasRemovePermit()){
            drawPermit = true;
            super.update();
        }
    }

    public boolean hasDrawPermit() {
        return drawPermit;
    }
}
