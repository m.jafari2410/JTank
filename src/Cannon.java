import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * This class models a Cannon. Cannon is a type of weapon of the tank and this class extends class Weapon.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 7.8.2018
 */
public class Cannon extends Weapon {

    /**
     * This is constructor and initials the fields.
     */
    public Cannon() {

        setBullet(new CannonBullet());
        try {
            setImageOfWeapon(ImageIO.read(new File("data/image/Cannon1.png")));
            setSecImageOfWeapon(ImageIO.read(new File("data/image/Cannon2.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        setFirstNumberOfBullets(50);
        setNumberOfBullets(getFirstNumberOfBullets());
        setPositionRCenterOfTank(new Coordinate(-30,-50));
        setAnchor(new Coordinate(32,47));
    }

}
