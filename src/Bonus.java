import java.awt.*;

/**
 * Class of bonuses
 * @author Mohammad Jafari
 * @version 2.0.0
 * @since 7.14.2018
 */

public class Bonus extends Passable{
    private Coordinate place;
    private boolean removePermit = false;

    public Bonus(Coordinate place) {
        this.place = place;
    }

    public void update(){
        Component component = Operator.intersects(this,new Coordinate(place.getX() - GameFrame.screenPosition.getX(), place.getY() - GameFrame.screenPosition.getY()));
        if (component instanceof Tank){
            Tank tank = (Tank) component;
            prize(tank);
            removePermit = true;
        }
    }

    public void prize(Tank tank) {
    }

    public boolean hasRemovePermit() {
        return removePermit;
    }

    public Coordinate getPlace() {
        Coordinate coordinate = new Coordinate(place.getX() - GameFrame.screenPosition.getX(), place.getY() - GameFrame.screenPosition.getY());
        return coordinate;
    }
}
