import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

/**
 * This class handles mouse and extends class MouseAdapter.
 *
 * @author Mohammad Jafari
 * @version 2.1
 * @since  7.9.2018
 */

public class MouseHandler extends MouseAdapter {
    private int mouseX;
    private int mouseY;
    private boolean continuesShoot;
    private boolean hasWeaponChanged;


    /**
     * When mouse moved.
     */
    @Override
    public void mouseMoved(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }

    /**
     * Each time that moused is pressed.
     */
    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1)
            continuesShoot = true;
    }

    /**
     * When mouse released.
     */
    @Override
    public void mouseReleased(MouseEvent e) {
        continuesShoot = false;
        hasWeaponChanged = false;
    }

    /**
     * When mouse clicked.
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON3)
            hasWeaponChanged = true;
    }

    /**
     * if mouse was dragged.
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        mouseX = e.getX();
        mouseY = e.getY();
    }

    /**
     * This method gets x position of the mouse
     * @return x of the mouse
     */
    public int getMouseX() {
        return mouseX;
    }

    /**
     * This method gets y position of the mouse
     * @return y of the mouse
     */
    public int getMouseY() {
        return mouseY;
    }


    /**
     * If player has shot continuesly.
     * @return true if player has shot.
     */
    public boolean isContinuesShoot() {
        return continuesShoot;
    }

    /**
     * @return true if player has changed the weapon.
     */
    public boolean hasWeaponChanged() {
        return hasWeaponChanged;
    }

    public void setWeaponChanged(){
        hasWeaponChanged = false;
    }

    public void setShootingDone() {
        continuesShoot = false;
    }
}
