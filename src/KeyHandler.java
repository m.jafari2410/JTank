import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * This class handles keys and extends class KeyAdapter.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 1.7.2018
 */
public class KeyHandler extends KeyAdapter  {
    private boolean keyUP, keyDOWN, keyRIGHT, keyLEFT;
    private boolean keyEnter;
    private static String  cheatCode = "";


    /**
     * When arrow keys are pressed.
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if(cheatCode.length() < 8){
            if (cheatCode == "")
                cheatCode = String.valueOf(e.getKeyChar());
            else
                cheatCode += e.getKeyChar();

        } else if (cheatCode.length() == 8){
            cheatCode = cheatCode.substring(1);
            cheatCode += e.getKeyChar();
        }

        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                keyUP = true;
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                keyDOWN = true;
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                keyLEFT = true;
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                keyRIGHT = true;
                break;
            case KeyEvent.VK_ENTER:
                keyEnter = true;
                break;
        }
    }

    /**
     * When keys are released.
     */
    @Override
    public void keyReleased(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
            case KeyEvent.VK_W:
                keyUP = false;
                break;
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_S:
                keyDOWN = false;
                break;
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_A:
                keyLEFT = false;
                break;
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_D:
                keyRIGHT = false;
                break;
        }
    }

    /**
     * If key up is pressed.
     */
    public boolean isKeyUP() {
        return keyUP;
    }

    /**
     * If key down is pressed.
     */
    public boolean isKeyDOWN() {
        return keyDOWN;
    }

    /**
     * If key right is pressed.
     */
    public boolean isKeyRIGHT() {
        return keyRIGHT;
    }

    /**
     * If key left is pressed.
     */
    public boolean isKeyLEFT() {
        return keyLEFT;
    }

    /**
     * If shift is pressed.
     * @return true if shift is pressed.
     */
    public boolean isKeyEnter() {
        return keyEnter;
    }

    public void setKeyShift(boolean keyShift) {
        this.keyEnter = keyShift;
    }

    public static void setCheatCode(String cheatCode) {
        KeyHandler.cheatCode = cheatCode;
    }

    public static String getCheatCode() {

        return cheatCode;
    }
}
