import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class UpgradeBox extends HiddenBonus {

    public UpgradeBox(Coordinate place, Destroyable destroyable) {
        super(place, destroyable);

        try {
            setImage(ImageIO.read(new File("data/image/upgrade.png")));
        } catch (IOException e) {
            e.printStackTrace();
        }

        setHeight(70);
        setWidth(70);
    }

    @Override
    public void prize(Tank tank) {
        tank.upgradeWeapon();
    }
}
