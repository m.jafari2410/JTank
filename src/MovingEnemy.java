import com.sun.org.apache.xpath.internal.operations.Bool;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * This class is about a moving enemy which extends class SmartEnemy and also has an extra method to move the enemy.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 7.14.2018
 */
public class MovingEnemy extends SmartEnemy {
    private int iterationOfMoves;

    /**
     * This is constructor and calls super constructor.
     *
     * @param positionInMap is position of enemy in map.
     */
    public MovingEnemy(Coordinate positionInMap) {
        super(positionInMap);
        try {
            setImage(ImageIO.read(new File("data/image/SmallEnemy.png")));
            setImageOfGun(ImageIO.read(new File("data/image/SmallEnemyGun.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        setBullet(new MovingEnemyBullet());

        setAnchor(new Coordinate(21, 21));

        setLimitNumberOfRepetitiveShoots(60);

        switch (Operator.levelOfGame){
            case 0:
                setHealth(80);
                break;
            case 1:
                setHealth(130);
                break;
            case 2:
                setHealth(200);
                break;
        }

    }

    /**
     * This method updates state of the moving enemy. This method overrides the method in class SmartEnemy.
     * Total operation is described here: The enemy moves to see the player and after that enemy shoots.
     *
     * @param playerCoordinate position of player tank.
     */
    @Override
    public void updateState(Coordinate playerCoordinate) {
        setDrawPermit(true);
        super.updateState(playerCoordinate);
        moveEnemy(playerCoordinate);
    }


    /**
     * This method moves enemy to have better position to see the player tank.
     *
     * @param playerCoordinate position of player tank.
     */
    public void moveEnemy(Coordinate playerCoordinate) {
        if (iterationOfMoves < 400){
            setPositionInMap(new Coordinate(getPositionInMap().getX() + 1, getPositionInMap().getY()));
            iterationOfMoves++;
        } else if (iterationOfMoves < 800) {
            setPositionInMap(new Coordinate(getPositionInMap().getX() - 1, getPositionInMap().getY()));
            iterationOfMoves++;
        } else if (iterationOfMoves < 1200) {
            iterationOfMoves++;
        } else
            iterationOfMoves = 0;
    }
}
