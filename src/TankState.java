/**
 * This class models state of the tank and listens to the keyboard and mouse to find out when an interrupt has occurred and
 * thus method update of this class is executed.
 *
 * @author Mohammad Jafari
 * @version 4
 * @since 7.9.2018
 */
public class TankState {
    private Tank tank;
    private MouseHandler mouseHandler;
    private KeyHandler keyHandler;
    //angle of the gun on the tank
    private double angleOfGun;
    //this variable is for shooting 1 time as 20 shoots created
    private int iterationOfIgnoreShoots = 0;
    private boolean isWin = false;

    /**
     * This is constructor and initials fields.
     *
     * @param tank the tank that it's state is watched
     */
    public TankState(Tank tank) {
        this.tank = tank;
        mouseHandler = new MouseHandler();
        keyHandler = new KeyHandler();
    }

    /**
     * This method is called each time that keyboard or mouse have changed.
     */
    public void update(Map map) {
        if (GameFrame.screenPosition.getY() + GameFrame.GAME_HEIGHT > 4900) {
            tank.setYPosRScreen(tank.getYPosRScreen() + 4);
            tank.setYPosRMap(tank.getYPosRMap() + 4);
            isWin = true;

        }else {
            tank.setAngleOfGun(calculateAngleOfGun());
            moveTank(map);
            //this method makes sure if the weapon is a machine gun then shoot 1 time as 20 times.
            if (mouseHandler.isContinuesShoot()) {
                if (tank.getActiveWeapon() instanceof MachineGun && iterationOfIgnoreShoots < 15)
                    iterationOfIgnoreShoots++;
                else if (iterationOfIgnoreShoots >= 15 || tank.getActiveWeapon() instanceof Cannon) {
                    shoot();
                    iterationOfIgnoreShoots = 0;
                }
                if (tank.getActiveWeapon() instanceof Cannon) {
                    mouseHandler.setShootingDone();
                }
            }

            if (mouseHandler.hasWeaponChanged() || keyHandler.isKeyEnter()) {
                new Audio("data/sound/tank/toggleWeapon.wav").startEffect();
                changeWeapon();
                mouseHandler.setWeaponChanged();
                keyHandler.setKeyShift(false);
            }
            if (CheatCode.isCheatCode(KeyHandler.getCheatCode()) != null)
                CheatCode.applyCode(tank, CheatCode.isCheatCode(KeyHandler.getCheatCode()));
        }
    }

    /**
     * This method calculate angle of the gun and returns it.
     *
     * @return angle of the gun
     */
    public double calculateAngleOfGun() {
        angleOfGun = Math.atan2((double) (mouseHandler.getMouseY() + 16 - tank.getCenterOfTank().getY()), (double) (mouseHandler.getMouseX() + 12 - tank.getCenterOfTank().getX()));
        return angleOfGun;
    }

    /**
     * This method moves the tank according to the keyHandler class.
     */
    public void moveTank(Map map) {
        //how many pixels move
        int x = 2;
        if (keyHandler.isKeyUP()) {
            if (Operator.intersects(tank, new Coordinate(tank.getXPosRScreen(), tank.getYPosRScreen() - 4)) == null)
                if (tank.getYPosRScreen() - x >= 0) {
                    int a = tank.getYPosRMap();
                    tank.setYPosRMap(tank.getYPosRMap() - x);
                    if ((tank.getYPosRScreen() > GameFrame.GAME_HEIGHT / 2 && a - tank.getYPosRScreen() + GameFrame.GAME_HEIGHT == 100 * map.getRows())
                            || tank.getYPosRMap() <= GameFrame.GAME_HEIGHT / 2 || 100 * map.getRows() - tank.getYPosRMap() <= GameFrame.GAME_HEIGHT)
                        tank.setYPosRScreen(tank.getYPosRScreen() - x);
                    else
                        tank.setYPosRMap(tank.getYPosRMap() - x);
                }
        }
        if (keyHandler.isKeyDOWN()) {
            if (Operator.intersects(tank, new Coordinate(tank.getXPosRScreen(), tank.getYPosRScreen() + 4)) == null)
                if (tank.getYPosRScreen() + x <= (GameFrame.GAME_HEIGHT - 86)) {
                    int a = tank.getYPosRMap();
                    tank.setYPosRMap(tank.getYPosRMap() + x);
                    if (tank.getYPosRScreen() < GameFrame.GAME_HEIGHT / 2 || tank.getYPosRMap() <= GameFrame.GAME_HEIGHT / 2 ||
                            a - tank.getYPosRScreen() + GameFrame.GAME_HEIGHT == 100 * map.getRows()
                            || 100 * map.getRows() - tank.getYPosRMap() <= GameFrame.GAME_HEIGHT)
                        tank.setYPosRScreen(tank.getYPosRScreen() + x);
                    else
                        tank.setYPosRMap(tank.getYPosRMap() + x);
                }
        }
        if (keyHandler.isKeyLEFT()) {
            if (Operator.intersects(tank, new Coordinate(tank.getXPosRScreen() - 4, tank.getYPosRScreen())) == null)
                if (tank.getXPosRScreen() - x >= 0) {
                    int a = tank.getXPosRMap();
                    tank.setXPosRMap(tank.getXPosRMap() - x);
                    if ((tank.getXPosRScreen() < GameFrame.GAME_WIDTH / 2 && a - tank.getXPosRScreen() + GameFrame.GAME_WIDTH == 100 * map.getColumns())
                            || tank.getXPosRMap() <= GameFrame.GAME_WIDTH / 2 || 100 * map.getColumns() - tank.getXPosRMap() <= GameFrame.GAME_WIDTH)
                        tank.setXPosRScreen(tank.getXPosRScreen() - x);
                    else
                        tank.setXPosRMap(tank.getXPosRMap() - x);

                }
        }
        if (keyHandler.isKeyRIGHT()) {
            if (Operator.intersects(tank, new Coordinate(tank.getXPosRScreen() + 4, tank.getYPosRScreen())) == null)
                if (tank.getXPosRScreen() + x <= (GameFrame.GAME_WIDTH - 90)) {
                    int a = tank.getXPosRMap();
                    tank.setXPosRMap(tank.getXPosRMap() + x);
                    if (tank.getXPosRScreen() < GameFrame.GAME_WIDTH / 2 || 100 * map.getColumns() - tank.getXPosRMap() <= GameFrame.GAME_WIDTH ||
                            a - tank.getXPosRScreen() + GameFrame.GAME_WIDTH == 100 * map.getColumns())
                        tank.setXPosRScreen(tank.getXPosRScreen() + x);
                    else
                        tank.setXPosRMap(tank.getXPosRMap() + x);
                }
        }

    }


    /**
     * This method shoots the target
     */
    public void shoot() {
        tank.shoot(mouseHandler.getMouseX(), mouseHandler.getMouseY());
    }

    /**
     * This methods calls change weapon method of class Tank
     */
    public void changeWeapon() {
        tank.changeWeapon();
    }

    /**
     * This method calls upgradeWeapon from class Tank.
     */
    public void upgradeWeapon() {
        tank.upgradeWeapon();
    }

    /**
     * This method gets the mouseHandler object
     *
     * @return mouseHandler object
     */
    public MouseHandler getMouseHandler() {
        return mouseHandler;
    }

    /**
     * This method gets the keyHandler object.
     *
     * @return keyHandler object
     */
    public KeyHandler getKeyHandler() {
        return keyHandler;
    }

    /**
     * This method gets the tank
     *
     * @return
     */
    public Tank getTank() {
        return tank;
    }

    public boolean isWin() {
        return isWin;
    }
}
