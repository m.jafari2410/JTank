import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class CannonShellsBox extends Bonus {

    public CannonShellsBox(Coordinate place) {
        super(place);

        try {
            setImage(ImageIO.read(new File("data/image/CannonFood.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setWidth(53);
        setHeight(44);
    }

    @Override
    public void prize(Tank tank) {
        tank.getCannonWeapon().increaseBullets(40);
        if (tank.getCannonWeapon().getNumberOfBullets() > tank.getCannonWeapon().getFirstNumberOfBullets())
            tank.getCannonWeapon().setFirstNumberOfBullets(tank.getCannonWeapon().getNumberOfBullets());
    }
}
