import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class is like soft wall with a different image
 * @author Mohamma Karimi
 * @version 1.0
 * @since 14.7.2018
 */
public class FinishWall extends SoftWall {
    public FinishWall(){
        setHealth(200);
        try {
            setImage(ImageIO.read(new File("data/image/fWall1.png")));
            setSecondImage(ImageIO.read(new File("data/image/fWall2.png")));
            setThirdImage(ImageIO.read(new File("data/image/fWall3.png")));
            setFourthImage(ImageIO.read(new File("data/image/fWall4.png")));
            setDeathImage(ImageIO.read(new File("data/image/dust.png")));
            setAfterDeathImage(ImageIO.read(new File("data/image/dust2.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setWidth(95);
    }
}
