import javax.swing.*;
import java.awt.*;

/**
 * This class handle errors
 * @author Mohamma Karimi
 * @version 1.0
 * @since 8.7.2018
 */

public class Errors {

    private Errors(){

    }

    /**
     * This method handle errors
     * @param error type of error
     */
    public static void handler(String error){
        switch (error){
            case "loadingError":
                Operator.showSimpleMessage("File loading error","File Loading error! Install game again","Error");
                System.exit(0);
                break;
            case "lookAndFeelError":
                Operator.showSimpleMessage("Look and feel error","Look and feel not found","Error");
                break;
        }
    }
}
