import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
/**
 * status of game
 * @author Mohammad Karimi
 * @version 2.1
 * @since 13.7.2018
 */
public class GraphicStatus {
    Image heart1;
    Image heart2;
    Image heart3;
    Image numberOfHeavyBullet;
    Image numberOfMachineGun;
    Image star;

    /**
     * constructor of class
     */
    public GraphicStatus(){
        try {
            heart1 = ImageIO.read(new File("data/image/health1.png"));
            heart2 = ImageIO.read(new File("data/image/health2.png"));
            heart3 = ImageIO.read(new File("data/image/health3.png"));
            numberOfHeavyBullet = ImageIO.read(new File("data/image/NumberOfHeavyBullet.png"));
            numberOfMachineGun = ImageIO.read(new File("data/image/NumberOfMachineGun.png"));
            star = ImageIO.read(new File("data/image/star.png"));
        } catch (Exception e){
            Errors.handler("loadingError");
        }
    }
}