import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class MachineGunShellsBox extends Bonus {

    public MachineGunShellsBox(Coordinate place) {
        super(place);
        try {
            setImage(ImageIO.read(new File("data/image/MashinGunFood.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setWidth(53);
        setHeight(35);
    }

    @Override
    public void prize(Tank tank) {
        tank.getMachineGunWeapon().increaseBullets(100);
        if (tank.getMachineGunWeapon().getNumberOfBullets() > tank.getMachineGunWeapon().getFirstNumberOfBullets())
            tank.getMachineGunWeapon().setFirstNumberOfBullets(tank.getMachineGunWeapon().getNumberOfBullets());
    }
}
