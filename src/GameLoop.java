import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * This class implements interface runnable and repetitively calls method update state
 * of tank or map and then calls the rendering method in class GameFrame.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 1.7.2018
 */
public class GameLoop implements Runnable {
    //frame per second of the screen
    public static final int FPS = Integer.MAX_VALUE;
    //frame of the game
    private GameFrame gameFrame;
    private Player player;
    private Map map = new Map();
    //list of enemies
    private ArrayList<Enemy> enemies;

    private ArrayList<Bonus> bonuses;

    /**
     * This is constructor and initials fields of this class
     *
     * @param frame frame of the game
     */
    public GameLoop(GameFrame frame) {
        gameFrame = frame;

        map.readMap();
        enemies = map.getEnemies();
        Operator.setEnemies(enemies);
        Operator.setInScreenMap(map.getInScreenMap());

        player.initPlayer();
        Operator.setTank(player.getState().getTank());

        bonuses = map.getBonuses();

        gameFrame.addKeyListener(player.getState().getKeyHandler());
        gameFrame.addMouseListener(player.getState().getMouseHandler());
        gameFrame.addMouseMotionListener(player.getState().getMouseHandler());
    }

    /**
     * This method is executed always and thus update and render.
     */
    @Override
    public void run() {
        boolean gameOver = false;
        while (!gameOver) {
            try {
                long start = System.currentTimeMillis();

                Coordinate playerTankCoordinate = new Coordinate(player.getState().getTank().getXPosRScreen(), player.getState().getTank().getYPosRScreen());

                if (player.getState().getTank().hasUpdatePermit())
                    for (Iterator<Enemy> iterator = enemies.iterator(); iterator.hasNext(); ) {
                        Enemy enemy = iterator.next();
                        if (enemy.isEnemyInScreen()) {
                            if (enemy.hasUpdatePermit())
                                enemy.updateState(playerTankCoordinate);
                        }
                        if (enemy.hasRemovePermit()) {
                            iterator.remove();
                        }
                    }

                for (Iterator<Bonus> iterator = bonuses.iterator(); iterator.hasNext(); ) {
                    Bonus bonus = iterator.next();
                    bonus.update();
                    if (bonus.hasRemovePermit())
                        iterator.remove();
                }

                if (player.getState().getTank().hasUpdatePermit())
                    player.getState().update(map);

                if (player.getState().getTank().hasRemovePermit())
                    gameOver = true;

                map.getInScreenMap().clear();
                gameFrame.render(player.getState(), map);

                long delay = (1000 / FPS) - (System.currentTimeMillis() - start);
                if (delay > 0)
                    Thread.sleep(delay);
            } catch (InterruptedException ex) {
            }
        }

        GraphicOperator.closeFrameAndAudio(player.getState().getTank().getEmergency(), gameFrame);
        MenuFrame.getInstance().init();
    }
}
