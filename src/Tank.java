/**
 * This class models a tank. It has two weapons and a life amount.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 7.8.2018
 */

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Tank extends Destroyable {

    Audio audio = new Audio("data/sound/tank/cannon.wav");
    // Position of tank relative to map
    private int xPosRMap;
    private int yPosRMap;

    // Position of tank relative to screen
    private int xPosRScreen;
    private int yPosRScreen;


    private Weapon cannonWeapon;
    private Weapon machineGunWeapon;
    private Weapon activeWeapon;
    private int life = 100;
    private BufferedImage imageOfTank;
    //maintaining list of shoots
    private ArrayList<Bullet> shoots;
    private Coordinate centerOfTank;
    private double angleOfGun;
    private double angleOfTank;

    private Audio emergency;

    private final int firstHealth = 250;


    public final int lengthOfTankGunTube = 53;

    public Audio getEmergency() {
        return emergency;
    }

    /**
     * This is constructor and initials fields of class
     */
    public Tank() {
        cannonWeapon = new Cannon();
        machineGunWeapon = new MachineGun();
        activeWeapon = cannonWeapon;
        centerOfTank = new Coordinate(50,47);
        setHealth(firstHealth);
        shoots = new ArrayList<Bullet>();
        emergency = new Audio("data/sound/emergency.wav");

        try {
            setImage(ImageIO.read(new File("data/image/tank.png")));
            setAfterDeathImage(ImageIO.read(new File("data/image/blood.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setShowNumberOfDeathImage(0);
        setShowNumberOfAfterDeathImage(100);

        setHeight(90);
        setWidth(95);
    }

    /**
     * This method does shooting.
     */
    public void shoot(int xTarget, int yTarget) {
        if (activeWeapon.getNumberOfBullets() > 0) {
            activeWeapon.decreaseBullets();
            try {
                shoots.add(activeWeapon.makeBullet((int) (centerOfTank.getX() + lengthOfTankGunTube * Math.cos(angleOfGun)), (int) (centerOfTank.getY() + lengthOfTankGunTube * Math.sin(angleOfGun)), xTarget + 7, yTarget + 13));
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            activeWeapon.getBullet().getShootVoice().startEffect();
        }
    }

    /**
     * This method gets x of tank relative to map
     *
     * @return x position of tank relative to map
     */
    public int getXPosRMap() {
        return xPosRMap;
    }

    /**
     * This method gets y of tank relative to map
     *
     * @return y position of tank relative to map
     */
    public int getYPosRMap() {
        return yPosRMap;
    }

    /**
     * This method gets the active weapon
     *
     * @return active weapon of tank
     */
    public Weapon getWeapon() {
        return activeWeapon;
    }

    /**
     * This method sets y of tank relative to map
     *
     * @param yPosRMap y of tank relative to map
     */
    public void setYPosRMap(int yPosRMap) {
        this.yPosRMap = yPosRMap;
    }


    /**
     * This method sets x of tank relative to map
     *
     * @param xPosRMap x of tank relative to map
     */
    public void setXPosRMap(int xPosRMap) {
        this.xPosRMap = xPosRMap;
    }

    /**
     * This method changes the weapon of the tank
     */
    public void changeWeapon() {
        if (activeWeapon == cannonWeapon)
            activeWeapon = machineGunWeapon;
        else
            activeWeapon = cannonWeapon;
    }

    /**
     * This upgrades active weapon
     */
    public void upgradeWeapon() {
        activeWeapon.increaseLevel();
    }

    /**
     * This Method gets damage amount of time
     *
     * @param bullet
     */
    public void getDamage(Bullet bullet) {
        life -= bullet.getDamageIntensity();
    }

    /**
     * This method does the repairing of the tank
     */
    public void repair() {
        life = 100;
    }

    /**
     * This method gets image of the tank
     *
     * @return
     */
    public Image getImageOfGun() {
        return activeWeapon.getImageOfWeapon();
    }

    /**
     * This method gets image of the tank
     *
     * @return
     */
    public Image getImageOfTank() {
        return getImage();
    }

    /**
     * This method sets image of tank
     *
     * @param imageOfTank
     */
    public void setImageOfTank(BufferedImage imageOfTank) {
        setImage(imageOfTank);
    }

    /**
     * @return x position relative to screen
     */
    public int getXPosRScreen() {
        return xPosRScreen;
    }

    /**
     * @return y position relative to screen
     */
    public int getYPosRScreen() {
        return yPosRScreen;
    }

    /**
     * This method sets x position of tank relative to the screen
     *
     * @param xPosRScreen x position relative to the screen
     */
    public void setXPosRScreen(int xPosRScreen) {
        centerOfTank.setX(centerOfTank.getX() + xPosRScreen - this.xPosRScreen);
        this.xPosRScreen = xPosRScreen;
    }

    /**
     * This method sets x position of tank relative to the screen
     *
     * @param yPosRScreen y position relative to the screen
     */
    public void setYPosRScreen(int yPosRScreen) {
        centerOfTank.setY(centerOfTank.getY() + yPosRScreen - this.yPosRScreen);
        this.yPosRScreen = yPosRScreen;
    }

    /**
     * This method gets the active weapon.
     *
     * @return active weapon of the tank.
     */
    public Weapon getActiveWeapon() {
        return activeWeapon;
    }

    /**
     * This method gets list of shots
     *
     * @return list of shots
     */
    public ArrayList<Bullet> getShoots() {
        return shoots;
    }

    /**
     * This method gets coordinate of center of the tank.
     * @return center coordinate of the tank.
     */
    public Coordinate getCenterOfTank() {
        return centerOfTank;
    }

    public void setAngleOfGun(double angleOfGun) {
        this.angleOfGun = angleOfGun;
    }

    public double getAngleOfGun() {
        return angleOfGun;
    }

    public Weapon getCannonWeapon() {
        return cannonWeapon;
    }

    public Weapon getMachineGunWeapon() {
        return machineGunWeapon;
    }

    public int getFirstHealth() {
        return firstHealth;
    }

    @Override
    public void makeSound() {

    }
}
