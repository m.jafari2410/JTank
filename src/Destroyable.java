import java.awt.*;

/**
 * This class models a destroyable component and extends class Impassable.
 *
 * @author Mohamma Jafari
 * @version 1.0
 * @since 7.9.2018
 */

public class Destroyable extends Impassable {
    //health of the destroyable component
    private int health;
    private Image deathImage;
    private Image afterDeathImage;
    private boolean removePermit = false;
    private boolean updatePermit = true;
    private int showIteration;
    private Audio destroyAudio;
    private int showNumberOfDeathImage = 20;
    private int showNumberOfAfterDeathImage = 50;

    /**
     * This method sets health of the destroyable component.
     * @param health health of the destroyable component.
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * This method gets health of destroyable component.
     * @return specified health.
     */
    public int getHealth() {
        return health;
    }

    /**
     * This method decreases health of a destroyable component.
     * @param damage specified damage to decrease health.
     */
    public void decreaseHealth(int damage) {
        health -= damage;
    }

    public void setDeathImage(Image deathImage) {
        this.deathImage = deathImage;
    }

    public Image getDeathImage() {
        return deathImage;
    }

    public void setAfterDeathImage(Image afterDeathImage) {
        this.afterDeathImage = afterDeathImage;
    }

    public Image getAfterDeathImage() {
        return afterDeathImage;
    }

    public boolean checkHealth() {
        if (getHealth() <= 0)
            return false;
        return true;
    }

    @Override
    public Image getImage() {
        if (checkHealth())
            return super.getImage();
        makeSound();
        updatePermit = false;
        if (showIteration < showNumberOfDeathImage){
            showIteration++;
            return getDeathImage();
        } else if (showIteration < showNumberOfAfterDeathImage) {
            showIteration++;
            return getAfterDeathImage();
        } else {
            removePermit = true;
            return getAfterDeathImage();
        }
    }

    public boolean hasRemovePermit() {
        return removePermit;
    }

    public boolean hasUpdatePermit() {
        return updatePermit;
    }

    public void makeSound() {
        destroyAudio.start(0);
    }

    public void setDestroyAudio(Audio destroyAudio) {
        this.destroyAudio = destroyAudio;
    }

    public void setShowNumberOfDeathImage(int showNumberOfDeathImage) {
        this.showNumberOfDeathImage = showNumberOfDeathImage;
    }

    public void setShowNumberOfAfterDeathImage(int showNumberOfAfterDeathImage) {
        this.showNumberOfAfterDeathImage = showNumberOfAfterDeathImage;
    }

    public void setRemovePermit(boolean removePermit) {
        this.removePermit = removePermit;
    }
}
