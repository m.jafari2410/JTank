import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * This class extends class bullets and models shots of the cannon gun.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.7.2018
 */

public class CannonBullet extends Bullet {
    public CannonBullet() {
        setShootVoice(new Audio("data/sound/tank/cannon.wav"));
        try {
            setImage(ImageIO.read(new File("data/image/HeavyBullet.png")));
            setExplosionImage(ImageIO.read(new File("data/image/HeavyFire.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setDamageIntensity(50);

        setWidth(23);
        setHeight(9);
    }
}
