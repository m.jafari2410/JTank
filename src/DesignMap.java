import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

/**
 * map designer class
 * @author Mohammad Karimi
 * @version 2.1
 * @since 14.7.2018
 */
public class DesignMap {
    JFrame frame1;
    JTextField height;
    JTextField width;
    JLabel label1;
    JLabel label2;
    JPanel panel1;
    int column = 1;
    int row = 1;
    //******************
    JFrame frame2;
    JLabel label3;
    JPanel panel2;
    JLabel label4, label5;
    JTextField textField1,textField2;
    JComboBox comboBox;
    JPanel panel3;
    JPanel panel4;
    int x = 1, y = 1,z = 1;
    char[][] map;

    /**
     * constructor of class
     */
    public DesignMap(){
        frame1 = new JFrame("Size of Map");
        height = new JTextField();
        height.setPreferredSize(new Dimension(30,25));
        width = new JTextField();
        width.setPreferredSize(new Dimension(30,25));
        label1 = new JLabel("  *");
        label1.setPreferredSize(new Dimension(20,20));
        label2 = new JLabel("Enter size and press \"ENTER\"");
        panel1 = new JPanel();
        panel1.add(label2);
        panel1.add(height);
        panel1.add(label1);
        panel1.add(width);
        frame1.add(panel1);
        width.addKeyListener(new PressEnter() );
        height.addKeyListener(new PressEnter() );
        frame1.setSize(180,60);
        frame1.setLocationRelativeTo(null);
        frame1.setUndecorated(true);
        frame1.setVisible(true);
        //***************************
        frame2 = new JFrame();
        label3 = new JLabel("Row: " + z);
        panel2 = new JPanel();
        panel2.add(label3);
        frame2.add(panel2);
        frame2.setLayout(new GridLayout(0,1,0,0));
        frame2.setSize(250,100);
        frame2.setLocationRelativeTo(null);
        frame2.setUndecorated(true);
        frame2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
                    row++;
                    updateLabel();
                }
            }
        });
        panel3 = new JPanel();
        label4 = new JLabel("From");
        panel3.add(label4);
        textField1 = new JTextField();
        textField1.setPreferredSize(new Dimension(30,25));
        textField1.setText("" + x);
        textField1.setEditable(false);
        panel3.add(textField1);
        label5 = new JLabel("To");
        panel3.add(label5);
        textField2 = new JTextField();
        textField2.setPreferredSize(new Dimension(30,25));
        textField2.addKeyListener(new PressEnter2());
        panel3.add(textField2);
        comboBox = new JComboBox();
        comboBox.addItem("Soil");
        comboBox.addItem("Soft wall");
        comboBox.addItem("hard Wall");
        comboBox.addItem("Stupid enemy");
        comboBox.addItem("Big enemy");
        comboBox.addItem("Small enemy");
        comboBox.addItem("Moving enemy");
        comboBox.addItem("Soft wall with repair box bonus");
        comboBox.addItem("Soft wall with upgrade box bonus");
        comboBox.addItem("Machine gun shells box bonus");
        comboBox.addItem("Cannon shells box bonus");
        comboBox.addKeyListener(new PressEnter2());
        panel4 = new JPanel();
        panel4.add(comboBox);
        frame2.add(panel3);
        frame2.add(panel4);
        GraphicOperator.setFrameIcon(frame1,"data/icon/menu/Design Map.png");
        GraphicOperator.setFrameIcon(frame2,"data/icon/menu/Design Map.png");
    }

    /**
     * key handler class
     */
    public class PressEnter extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                if (!width.getText().equals("") && !height.getText().equals("") && Integer.valueOf(width.getText()) >= 14 && Integer.valueOf(height.getText()) >= 14) {
                    column = Integer.valueOf(width.getText());
                    row = Integer.valueOf(height.getText());
                    map = new char[row][column];
                    frame1.dispose();
                    frame2.setVisible(true);
                }
            }
            else if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
                frame1.dispose();
            }
        }
    }

    /**
     * this method update a label in screen
     */
    public void updateLabel(){
        label3.setText("Row: " + z);
        frame2.revalidate();
        frame2.repaint();
    }

    /**
     * keyHandler class2
     */
    public class PressEnter2 extends KeyAdapter{
        @Override
        public void keyPressed(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_ENTER){
                if (textField2.getText().equals("")){

                } else {
                    y = Integer.valueOf(textField2.getText());
                    if (y > column || y < x){

                    } else {
                        char c = 'g';
                        switch (comboBox.getSelectedIndex()){
                            case 0:
                                c = 'g';
                                break;
                            case 1:
                                c = 'w';
                                break;
                            case 2:
                                c = 'h';
                                break;
                            case 3:
                                c = 's';
                                break;
                            case 4:
                                c = 'b';
                                break;
                            case 5:
                                c = 't';
                                break;
                            case 6:
                                c = 'm';
                                break;
                            case 7:
                                c = 'q';
                                break;
                            case 8:
                                c = 'e';
                                break;
                            case 9:
                                c = 'i';
                                break;
                            case 10:
                                c = 'o';
                                break;

                        }
                        for (int i = x ; i <= y ; i++){
                            if (z == 1 && i == 1)
                                map[z-1][i-1] = 'g';
                            else
                                map[z-1][i-1] = c;
                        }
                        if (y == column){
                            x = 1;
                            if (z != row)
                                z++;
                            else {
                                writeInFile();
                                frame2.dispose();
                            }
                        } else {
                            x = y + 1;
                        }
                        update();
                    }
                }
            }
            else if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
                frame2.dispose();
            }
        }
    }

    /**
     * update first textfield
     */
    public void updateTextField(){
        textField1.setText("" + x);
        textField2.setText("");
    }

    /**
     * update frame
     */
    public void update(){
        updateTextField();
        updateLabel();

    }

    /**
     * make map
     */
    public void writeInFile(){

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("data/file/customMap.txt"));
            for (int i = 0 ; i < row ; i++){
                for (int j = 0 ; j < column ; j++){
                    if (i == 0 && j == 0)
                        writer.write(map[i][j]);
                    else
                        writer.append(map[i][j]);
                }
                if (i != row -1)
                    writer.append("\r\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

