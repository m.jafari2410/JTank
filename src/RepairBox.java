import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Repair boxes are boxes which repairs the tank.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.7.2018
 */
public class RepairBox extends HiddenBonus {

    public RepairBox(Coordinate place, Destroyable destroyable) {
        super(place, destroyable);
        try {
            setImage(ImageIO.read(new File("data/image/RepairFood2.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setHeight(40);
        setWidth(63);
    }

    @Override
    public void prize(Tank tank) {
        if (tank.getHealth() < tank.getFirstHealth())
            tank.setHealth(tank.getFirstHealth());
    }
}
