import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Information of map
 *
 * @author Mohammad Karimi
 * @version 2.1
 * @since 7.7.2018
 */

public class Map {
    private Scanner scanner;
    //number of rows and columns of map
    private  int rows = 50;
    private  int columns = 30;
    private Component[][] map;
    //HashMap maintains present components of screen and theirs positions.
    private HashMap<Coordinate, Component> inScreenMap;
    private ArrayList<Enemy> enemies;
    private ArrayList<Bonus> bonuses;

    /**
     * This is constructor and reads map.txt file.
     */
    public Map() {
        map = new Component[rows][columns];
        inScreenMap = new HashMap<Coordinate, Component>();
        enemies = new ArrayList<Enemy>();
        bonuses = new ArrayList<Bonus>();
    }

    /**
     * This method reads the .txt file and creates map.
     */
    public void readMap() {
        String address = "";
        if (new File("data/file/customMap.txt").exists())
            address = "data/file/customMap.txt";
        else {
            if (Operator.levelOfGame == 0)
                address = "data/file/map.txt";
            else if (Operator.levelOfGame == 1)
                address = "data/file/map2.txt";
            else if (Operator.levelOfGame == 2)
                address = "data/file/map3.txt";
        }
        try {
            scanner = new Scanner(new File(address));
        } catch (Exception e){
            Errors.handler("loadingError");
        }
        setRowsAndColumns(address);
        String string;
        int i = 0;
        while (scanner.hasNextLine()) {
            string = scanner.next();
            if (string != "") {
                for (int j = 0; j < columns; j++) {
                    map[i][j] = makeComponent(string.toCharArray()[j], i, j);
                }
                i++;
            }
        }
    }

    /**
     * This method returns a component in according to the input char.
     *
     * @param c type of component
     * @return component
     */
    public Component makeComponent(char c, int rows, int columns) {
        switch (c) {
            case 'w':
                return new SoftWall();
            case 'h':
                return new HardWall();
            case 'g':
                return null;
            case 's':
                enemies.add(new StupidEnemy(new Coordinate(100 * columns, 100 * rows)));
                return null;
            case 'b':
                enemies.add(new BigEnemy(new Coordinate(100 * columns, 100 * rows)));
                return null;
            case 'm':
                enemies.add(new MovingEnemy(new Coordinate(100 * columns, 100 * rows)));
                return null;
            case 't':
                enemies.add(new SmallEnemy(new Coordinate(100 * columns, 100 * rows)));
                return null;
            case 'q':
                SoftWall softWall = new SoftWall();
                bonuses.add(new RepairBox(new Coordinate(100 * columns, 100 * rows), softWall));
                return softWall;
            case 'v':
                BigEnemy bigEnemy = new BigEnemy(new Coordinate(100 * columns, 100 * rows));
                enemies.add(bigEnemy);
                bonuses.add(new RepairBox(new Coordinate(100 * columns, 100 * rows), bigEnemy));
                return null;
            case 'e':
                softWall = new SoftWall();
                bonuses.add(new UpgradeBox(new Coordinate(100 * columns, 100 * rows), softWall));
                return softWall;
            case 'n':
                bigEnemy = new BigEnemy(new Coordinate(100 * columns, 100 * rows));
                enemies.add(bigEnemy);
                bonuses.add(new UpgradeBox(new Coordinate(100 * columns, 100 * rows), bigEnemy));
                return null;
            case 'r':
                SmallEnemy smallEnemy = new SmallEnemy(new Coordinate(100 * columns, 100 * rows));
                enemies.add(smallEnemy);
                bonuses.add(new UpgradeBox(new Coordinate(100 * columns, 100 * rows), smallEnemy));
                return null;
            case 'i':
                bonuses.add(new MachineGunShellsBox(new Coordinate(100 * columns, 100 * rows)));
                return null;
            case 'o':
                bonuses.add(new CannonShellsBox(new Coordinate(100 * columns, 100 * rows)));
                return null;
            case 'f':
                return new FinishWall();
            default:
                return null;
        }
    }

    /**
     * This method returns map as a 2D array.
     *
     * @return map
     */
    public Component[][] getMap() {
        return map;
    }

    /**
     * @return rows of array of map
     */
    public int getRows() {
        return rows;
    }

    /**
     * @return columns of array of map
     */
    public int getColumns() {
        return columns;
    }

    /**
     * This method puts new element in hash map.
     *
     * @param coordinate coordinate of component
     * @param component  component in map
     */
    public void putInScreenMap(Coordinate coordinate, Component component) {
        inScreenMap.put(coordinate, component);
    }

    /**
     * @return hash map array list of in screen components.
     */
    public HashMap<Coordinate, Component> getInScreenMap() {
        return inScreenMap;
    }

    /**
     * This method gets list of enemies.
     *
     * @return list of enemies.
     */
    public ArrayList<Enemy> getEnemies() {
        return enemies;
    }

    public ArrayList<Bonus> getBonuses() {
        return bonuses;
    }

    public void setRowsAndColumns(String address){
        try{
            File file =new File(address);

            FileReader fr = new FileReader(file);
            LineNumberReader lnr = new LineNumberReader(fr);

            int linenumber = 1;
            columns = lnr.readLine().length();
            while (lnr.readLine() != null){
                linenumber++;
            }

            rows = linenumber;
            map = new Component[rows][columns];
            lnr.close();
        } catch (Exception e){

        }
    }
}

