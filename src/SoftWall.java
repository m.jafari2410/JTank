import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class models a soft wall that can be destroyed.
 *
 * @author Mohamma Jafari
 * @version 2.0
 * @since 7.9.2018
 */
public class SoftWall extends Destroyable {
    private Image secondImage;
    private Image thirdImage;
    private Image fourthImage;
    private int i = 1;

    public SoftWall() {
        setHealth(200);
        try {
            ArrayList<String> softWallNames = new ArrayList<>() ;
            softWallNames.add("softWall01.png");
            softWallNames.add("softWall02.png");
            Random i = new Random();
            int n = i.nextInt(100) + 0;
            String name = softWallNames.get(n % 2);
            setImage(ImageIO.read(new File("data/image/" + name)));

            secondImage = ImageIO.read(new File("data/image/softWall1.png"));
            thirdImage = ImageIO.read(new File("data/image/softWall2.png"));
            fourthImage = ImageIO.read(new File("data/image/softWall3.png"));
            setDeathImage(ImageIO.read(new File("data/image/dust.png")));
            setAfterDeathImage(ImageIO.read(new File("data/image/dust2.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setWidth(95);
    }

    @Override
    public Image getImage() {
        if (getHealth() > 150)
            return super.getImage();
        else if (getHealth() > 100)
            return secondImage;
        else if (getHealth() > 50)
            return thirdImage;
        else if (getHealth() > 0)
            return fourthImage;
        else{
            if (i < 20){
                i++;
                return getDeathImage();
            }
            if (i < 30){
                i++;
                return getAfterDeathImage();
            }
            setRemovePermit(true);
            return null;
        }
    }

    public void setSecondImage(Image secondImage) {
        this.secondImage = secondImage;
    }

    public void setThirdImage(Image thirdImage) {
        this.thirdImage = thirdImage;
    }

    public void setFourthImage(Image fourthImage) {
        this.fourthImage = fourthImage;
    }
}
