/**
 * This class models a player that has a tank. The state of the tank is controlled and watched by class TankState.
 *
 * @author Mohammad Jafari
 * @version 1.7.2018
 */
public class Player {
    private static TankState state;

    public static void initPlayer(){
        state = new TankState(new Tank());
    }

    public static TankState getState() {
        return state;
    }
}
