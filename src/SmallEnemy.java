import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class SmallEnemy extends SmartEnemy {
    /**
     * This is constructor and calls super constructor.
     *
     * @param positionInMap is position of enemy in map.
     */
    public SmallEnemy(Coordinate positionInMap) {
        super(positionInMap);

        try {
            setImage(ImageIO.read(new File("data/image/SmallEnemyBody.png")));
            setImageOfGun(ImageIO.read(new File("data/image/SmallEnemyGun.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setAnchor(new Coordinate(21, 21));

        setBullet(new SmallEnemyBullet());

        setLimitNumberOfRepetitiveShoots(30);

        switch (Operator.levelOfGame){
            case 0:
                setHealth(100);
                break;
            case 1:
                setHealth(150);
                break;
            case 2:
                setHealth(230);
                break;
        }
    }
}
