/**
 * This class is used to maintain pair of x and y position of things.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 7.9.2018
 */
public class Coordinate {
    private int x;
    private int y;

    /**
     * This is constructor and initials fields of class.
     * @param x is x position
     * @param y is y position
     */
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Overloaded constructor.
     */
    public Coordinate() {
    }

    /**
     * @return x position
     */
    public int getX() {
        return x;
    }

    /**
     * @return y position
     */
    public int getY() {
        return y;
    }

    /**
     * This method sets x position
     * @param x x position
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * This method sets y position
     * @param y y position
     */
    public void setY(int y) {
        this.y = y;
    }
}
