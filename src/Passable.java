/**
 * This class models a passable component and extends class component.
 *
 * @author Mohamma Jafari
 * @version 1.0
 * @since 7.9.2018
 */

public class Passable extends Component {

}
