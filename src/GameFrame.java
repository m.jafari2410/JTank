import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageInputStream;
import javax.swing.*;
import javax.tools.Tool;

/**
 * This class has been retrieved from "http://ceit.aut.ac.ir".
 *
 * @author Mohammad Jafari
 * @version 3.0
 * @since 7.8.2018
 */

public class GameFrame extends JFrame {
    //size of the window
    public static int GAME_HEIGHT = 720;                  // 720p game resolution
    public static int GAME_WIDTH = 16 * GAME_HEIGHT / 9;  // wide aspect ratio

    public static Coordinate screenPosition = new Coordinate(0, 0);

    private boolean firstRun = true;

    private BufferStrategy bufferStrategy;
    private BufferedImage cursorImage = null;
    private BufferedImage background;

    private GraphicStatus graphicStatus;


    /**
     * This is constructor and sets details of the game frame
     *
     * @param title is name of the frame
     */
    public GameFrame(String title) {
        super(title);
        GraphicOperator.setFrameIcon(this, "data/icon/gameIcon.jpg");

        try {
            background = ImageIO.read(new File("data/image/soil.png"));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        setResizable(false);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE)
                    PauseMenu.getInstance().init(GameFrame.this);
            }
        });
        GAME_WIDTH = screenSize.width;
        GAME_HEIGHT = screenSize.height;

        try {
            //this image is used to replace to cursor windows default icon
            cursorImage = ImageIO.read(new File("data/image/gun-pointer.png"));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        graphicStatus = new GraphicStatus();

    }

    /**
     * This must be called once after the JFrame is shown:
     * frame.setVisible(true);
     * and before any rendering is started.
     */
    public void initBufferStrategy() {
        // Triple-buffering
        createBufferStrategy(3);
        bufferStrategy = getBufferStrategy();
    }


    /**
     * Game rendering with triple-buffering using BufferStrategy.
     */
    public void render(TankState state, Map map) {
        // Get a new graphics context to render the current frame
        Graphics2D graphics = (Graphics2D) bufferStrategy.getDrawGraphics();
        try {
            // Do the rendering
            doRendering(graphics, state, map);
        } finally {
            // Dispose the graphics, because it is no more needed
            graphics.dispose();
        }
        // Display the buffer
        bufferStrategy.show();
        // Tell the system to do the drawing NOW;
        // otherwise it can take a few extra ms and will feel jerky!
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * Rendering all game elements based on the game state.
     */
    private void doRendering(Graphics2D g2d, TankState state, Map map) {

        g2d.drawImage(background, 0, 0, null);

        drawMap(g2d, state, map);
        if (firstRun) {
            CheatCode.addCheatCode("uhealthy");
            CheatCode.addCheatCode("fhealthy");
            CheatCode.addCheatCode("fbullet");
            CheatCode.addCheatCode("ubullet");
            CheatCode.addCheatCode("uweapon");
            //setting icon of cursor
            Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point(0, 0), "target");
            this.setCursor(cursor);
            firstRun = false;
        }

        for (Bonus bonus : map.getBonuses()) {
            if (!(bonus instanceof HiddenBonus))
                g2d.drawImage(bonus.getImage(), bonus.getPlace().getX(), bonus.getPlace().getY(), null);
            else {
                HiddenBonus hiddenBonus = (HiddenBonus) bonus;
                if (hiddenBonus.hasDrawPermit()) {
                    g2d.drawImage(bonus.getImage(), bonus.getPlace().getX(), bonus.getPlace().getY(), null);
                }
            }
        }

        AffineTransform rotationOfWeapon = AffineTransform.getTranslateInstance(state.getTank().getActiveWeapon().getPosition(0, state.getTank().getCenterOfTank()).getX(), state.getTank().getActiveWeapon().getPosition(0, state.getTank().getCenterOfTank()).getY());
        rotationOfWeapon.rotate(state.getTank().getAngleOfGun(), state.getTank().getActiveWeapon().getAnchor().getX(), state.getTank().getActiveWeapon().getAnchor().getY());

        for (Enemy enemy : map.getEnemies()) {
            if (enemy.isDrawable()) {
                if (enemy instanceof StupidEnemy) {
                    if (enemy.hasUpdatePermit())
                        g2d.drawImage(enemy.getImage(), enemy.getAffineTransform(), null);
                    else
                        g2d.drawImage(enemy.getImage(), enemy.getPositionInScreen().getX() - 50, enemy.getPositionInScreen().getY() - 120, null);
                } else {
                    drawShots(g2d, ((SmartEnemy) enemy).getShoots(), map);
                    g2d.drawImage(enemy.getImage(), enemy.getPositionInScreen().getX(), enemy.getPositionInScreen().getY(), null);
                    if (enemy.hasUpdatePermit())
                        g2d.drawImage(((SmartEnemy) enemy).getImageOfGun(), enemy.getAffineTransform(), null);
                }
            }
        }

        if (state.getTank().hasUpdatePermit()) {
            g2d.drawImage(state.getTank().getImage(), state.getTank().getXPosRScreen(), state.getTank().getYPosRScreen(), null);
            g2d.drawImage(state.getTank().getImageOfGun(), rotationOfWeapon, null);
        } else{
            g2d.drawImage(state.getTank().getImage(), 70, 0, null);
            try {
                g2d.drawImage(ImageIO.read(new File("data/image/gameOver.png")), 500, 200, null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        drawShots(g2d, state.getTank().getShoots(), map);

        drawStatus(g2d, state.getTank());
        if (state.getTank().getHealth() <= 50)
            state.getTank().getEmergency().startFromFirst();
        else {
            state.getTank().getEmergency().clip.close();
        }

        if (state.isWin()){
            try {
                g2d.drawImage(ImageIO.read(new File("data/image/win.png")), 500, 200, null);
            } catch (IOException e) {
                Errors.handler("loadingError");
            }
        }
    }

    /**
     * This method draws the map according to state of the tank.
     *
     * @param g2d   drawer
     * @param state is state of tank
     * @param map   map of the game
     */
    private void drawMap(Graphics2D g2d, TankState state, Map map) {

        // Start position of screen relative to the map
        int x = state.getTank().getXPosRMap() - 200 > 0 ? state.getTank().getXPosRMap() - 200 : 0;
        int y = state.getTank().getYPosRMap() - 200 > 0 ? state.getTank().getYPosRMap() - 200 : 0;


        // End position of screen relative to the map
        int x1 = GameFrame.GAME_WIDTH + x > map.getColumns() * 100 ? map.getColumns() * 100 : GameFrame.GAME_WIDTH + x;
        int y1 = GameFrame.GAME_HEIGHT + y >= map.getRows() * 100 ? map.getRows() * 100 : GameFrame.GAME_HEIGHT + y;

        if (y1 >= map.getRows() * 100) {
            y1 = map.getRows() * 100;
            y = y1 - GameFrame.GAME_HEIGHT;
        }

        if (x1 >= map.getColumns() * 100) {
            x1 = map.getColumns() * 100;
            x = x1 - GameFrame.GAME_WIDTH;
        }

        //set screen position relative to the map
        screenPosition.setX(x);
        screenPosition.setY(y);

        // Amount of previous blocks need to be drawn
        float startFloatX = (x / 100f) - (x / 100);
        float startFloatY = (y / 100f) - (y / 100);

        // Amount of next blocks need to be drawn
        float endFloatX = (x1 / 100f) - (x1 / 100);
        float endFloatY = (y1 / 100f) - (y1 / 100);

        // Number of last block in screen
        int b = endFloatX == 0 ? x1 / 100 : (x1 / 100) + 1;
        int c = endFloatY == 0 ? y1 / 100 : (y1 / 100) + 1;

        // i1,j1 are numbers of rows and columns of blocks
        int i1 = 0;
        for (int i = y / 100; i < c; i++) {
            int j1 = 0;
            for (int j = x / 100; j < b; j++) {
                if (map.getMap()[i][j] != null) {
                    if (map.getMap()[i][j].getImage() == null) {
                        map.getMap()[i][j] = null;
                        g2d.drawImage(background, 100 * j1 - (int) (startFloatX * 100), 100 * i1 - (int) (startFloatY * 100), null);
                        map.putInScreenMap(new Coordinate(100 * j1 - (int) (startFloatX * 100), 100 * i1 - (int) (startFloatY * 100)), map.getMap()[i][j]);
                    } else {
                        g2d.drawImage(map.getMap()[i][j].getImage(), 100 * j1 - (int) (startFloatX * 100), 100 * i1 - (int) (startFloatY * 100), null);
                        map.putInScreenMap(new Coordinate(100 * j1 - (int) (startFloatX * 100), 100 * i1 - (int) (startFloatY * 100)), map.getMap()[i][j]);
                    }
                } else {
                    g2d.drawImage(background, 100 * j1 - (int) (startFloatX * 100), 100 * i1 - (int) (startFloatY * 100), null);
                    map.putInScreenMap(new Coordinate(100 * j1 - (int) (startFloatX * 100), 100 * i1 - (int) (startFloatY * 100)), map.getMap()[i][j]);
                }
                j1++;
            }
            i1++;
        }
    }

    /**
     * This method iterates on list of shoots and draws them one after another.
     *
     * @param g2d    object of class Graphics2D.
     * @param shoots list of shoots.
     * @param map    is map of the game.
     */
    public void drawShots(Graphics2D g2d, ArrayList<Bullet> shoots, Map map) {
        Bullet bullet;
        for (Iterator<Bullet> iterator = shoots.iterator(); iterator.hasNext(); ) {
            bullet = iterator.next();
            if (bullet.hasRemoved()) {
                iterator.remove();
            }

            //updating position of bullet
            bullet.updatePosition();

            //rotation of bullet to the target
            AffineTransform rotationOfBullet = AffineTransform.getTranslateInstance(bullet.getXPos(), bullet.getYPos());
            rotationOfBullet.rotate(bullet.getAngle(), 5, 5);

            //checks if next position of the bullet has no problem with going on
            if (!Operator.checkBulletPath(new Coordinate(bullet.getNextXPos(), bullet.getNextYPos()), bullet)) {
                g2d.drawImage(bullet.getExplosionImage(), bullet.getXPos() - 30, bullet.getYPos() - 20, null);
                bullet.setRemoved(true);
            } else
                g2d.drawImage(bullet.getImage(), rotationOfBullet, null);
        }
    }

    public void drawStatus(Graphics2D g2d, Tank tank) {
        drawHearts(g2d, tank);
        drawNumberOfBullets(g2d, tank);
    }

    public void drawHearts(Graphics2D g2d, Tank tank) {
        int i = 0;
        int n = tank.getFirstHealth() / 5;
        for (i = 0; i < tank.getHealth() / n; i++) {
            if (i > 4)
                break;
            g2d.drawImage(graphicStatus.heart1, 1050 + 60 * i, 20, null);
        }
        if (tank.getHealth() % n != 0 && i <= 4) {
            if (tank.getHealth() % n > 0 && tank.getHealth() % n <= n / 2) {
                g2d.drawImage(graphicStatus.heart3, 1050 + 60 * i, 20, null);
            } else if (tank.getHealth() % n > n / 2 && tank.getHealth() % n <= n - 1) {
                g2d.drawImage(graphicStatus.heart2, 1050 + 60 * i, 20, null);
            }
        }

    }

    public void drawNumberOfBullets(Graphics2D g2d, Tank tank) {
        g2d.drawImage(graphicStatus.numberOfHeavyBullet, 5, 10, null);
        if (tank.getCannonWeapon().getLevel() == 2)
            g2d.drawImage(graphicStatus.star, 5, 15, null);
        String numberOfBullet = String.valueOf(tank.getCannonWeapon().getNumberOfBullets());
        g2d.setFont(new Font(this.getToolkit().getFontList()[2], Font.ITALIC + Font.BOLD, 30));
        g2d.setColor(Color.YELLOW);
        g2d.drawChars(numberOfBullet.toCharArray(), 0, numberOfBullet.length(), 35, 65);

        g2d.drawImage(graphicStatus.numberOfMachineGun, 5, 75, null);
        if (tank.getMachineGunWeapon().getLevel() == 2)
            g2d.drawImage(graphicStatus.star, 5, 95, null);
        String numberOfMachineGun = String.valueOf(tank.getMachineGunWeapon().getNumberOfBullets());
        g2d.drawChars(numberOfMachineGun.toCharArray(), 0, numberOfMachineGun.length(), 35, 135);
    }
}
