import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.*;
/**
 * Information
 * @author Mohammad Karimi
 * @version 2.1
 * @since 8.7.2018
 */
class AboutFrame
{
    /**
     * Constructor of class
     */
    public AboutFrame()
    {
        GraphicOperator.setLookAndFeel(GraphicOperator.getLookAndFeelsList()[1]);
        final JTextArea edit = new JTextArea(21, 30);
        edit.setEditable(false);
        Font font = new Font("Serif",Font.PLAIN, 15);
        edit.setFont(font);
        edit.setForeground(Color.BLUE);
        JButton about = new JButton("About");
        JButton help = new JButton("Help");
        JButton cheatCode = new JButton("Cheat code");
        JButton map = new JButton("Map help");
        about.addActionListener( new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    FileReader reader = new FileReader( "data/file/about.txt" );
                    BufferedReader br = new BufferedReader(reader);
                    edit.read( br, null );
                    br.close();
                    edit.requestFocus();
                }
                catch(Exception e2) {
                    Errors.handler("loadingError");
                }
            }
        });
        about.setToolTipText("About");
        help.addActionListener( new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    FileReader reader = new FileReader( "data/file/help.txt" );
                    BufferedReader br = new BufferedReader( reader );
                    edit.read( br,null );
                    br.close();
                    edit.requestFocus();
                }
                catch(Exception e2) {
                    Errors.handler("loadingError");
                }
            }
        });
        help.setToolTipText("Help");
        cheatCode.addActionListener( new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    FileReader reader = new FileReader( "data/file/cheatCode.txt" );
                    BufferedReader br = new BufferedReader( reader );
                    edit.read( br,null );
                    br.close();
                    edit.requestFocus();
                }
                catch(Exception e2) {
                    Errors.handler("loadingError");
                }
            }
        });
        cheatCode.setToolTipText("Cheat codes");
        map.addActionListener( new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try
                {
                    FileReader reader = new FileReader( "data/file/educationalMap.txt" );
                    BufferedReader br = new BufferedReader(reader);
                    edit.read( br, null );
                    br.close();
                    edit.requestFocus();
                }
                catch(Exception e2) {
                    Errors.handler("loadingError");
                }
            }
        });
        map.setToolTipText("Map");
        JPanel buttons = new JPanel();
        buttons.setLayout(new GridLayout(1,0,20,50));
        buttons.add(about);
        buttons.add(help);
        buttons.add(cheatCode);
        buttons.add(map);

        JFrame frame = new JFrame("About and help");

        GraphicOperator.setFrameIcon(frame,"data/icon/menu/info.png");
        frame.getContentPane().add( new JScrollPane(edit), BorderLayout.NORTH );
        frame.getContentPane().add(buttons, BorderLayout.SOUTH);
        frame.setSize(500,500);
        frame.setLocationRelativeTo( null );
        frame.setResizable(false);
        frame.setVisible(true);
    }
}