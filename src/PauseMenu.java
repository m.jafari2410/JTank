import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/**
 * This class use for pause menu
 * @author Mohamma Karimi
 * @version 1.0
 * @since 9.7.2018
 */

public class PauseMenu {

    private JFrame pauseMenuFrame ;
    private JLabel background = new JLabel();
    private ArrayList<JButton> buttons = new ArrayList<JButton>();
    private static PauseMenu pauseMenu = null;
    private static ArrayList<JButton> pauseMenuButtons = new ArrayList<JButton>();
    JFrame motherFrame;

    private PauseMenu(){}

    /**
     * init(show) pause menu
     * @param motherFrame
     */
    public void init(JFrame motherFrame){

        this.motherFrame = motherFrame;
        pauseMenuFrame = new JFrame("Pause");
        GraphicOperator.setFrameIcon(pauseMenuFrame,"data/icon/gameIcon.jpg");
        pauseMenuFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        pauseMenuFrame.setUndecorated(true);
        pauseMenuFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pauseMenuFrame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE){
                    buttons.clear();
                    pauseMenuFrame.dispose();
                }
            }
        });


        pauseMenuFrame.setLayout(new BorderLayout());
        background = new JLabel(new ImageIcon("data/image/menuBackground.jpg"));

        pauseMenuFrame.add(background);
        create();

        GraphicOperator.setFocus(pauseMenuFrame);
        pauseMenuFrame.setVisible(true);
    }

    private void create(){
        buttons = pauseMenuButtons();
        for (int i = 0 ; i < buttons.size() ; i++)
            background.add(buttons.get(i));
    }
    public static PauseMenu getInstance(){
        if (pauseMenu == null)
            pauseMenu = new PauseMenu();
        return pauseMenu;
    }
    private ArrayList<JButton> pauseMenuButtons(){
        JButton resume = new JButton();
        JButton menu = new JButton();
        JButton saveAndExit = new JButton();
        buttons.add(resume);
        resume.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttons.clear();
                pauseMenuFrame.dispose();
            }
        });
        buttons.add(menu);
        menu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MenuFrame.getInstance().init();
                buttons.clear();
                pauseMenuFrame.dispose();
                motherFrame.dispose();
            }
        });
        buttons.add(saveAndExit);
        saveAndExit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                buttons.clear();
                motherFrame.dispose();
                pauseMenuFrame.dispose();
                Operator.showSimpleMessage("Save","Successfully saved.","Info");
                System.exit(0);
            }
        });
        GraphicOperator.setButtons(buttons,pauseMenuButtonEnteredName(),pauseMenuButtonExitedName(),"data/icon/pauseMenu/",120);
        return buttons;
    }

    private ArrayList<String> pauseMenuButtonEnteredName(){
        ArrayList<String> enteredName = new ArrayList<>();
        enteredName.add("Resume");
        enteredName.add("Menu");
        enteredName.add("Exit And Save");
        return enteredName;

    }

    private ArrayList<String> pauseMenuButtonExitedName(){
        ArrayList<String> exitedName = new ArrayList<>();
        exitedName.add("Resume2");
        exitedName.add("Menu2");
        exitedName.add("Exit And Save2");
        return exitedName;
    }
}
