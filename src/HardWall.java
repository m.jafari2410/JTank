import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class models a hard wall that cannot be destroyed.
 *
 * @author Mohamma Jafari
 * @version 1.0
 * @since 7.9.2018
 */

public class HardWall extends Impassable {
    public HardWall() {
        ArrayList<String> hardWallNames = new ArrayList<>() ;
        hardWallNames.add("hardWall1.png");
        hardWallNames.add("hardWall2.png");
        hardWallNames.add("hardWall3.png");
        hardWallNames.add("hardWall4.png");
        Random i = new Random();
        int n = i.nextInt(100) + 0;
        String name = hardWallNames.get(n % 4);
        try {
            setImage(ImageIO.read(new File("data/image/" + name)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        setHeight(95);
        setWidth(95);
    }
}
