import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class MovingEnemyBullet extends Bullet {
    public MovingEnemyBullet() {
        try {
            setImage(ImageIO.read(new File("data/image/EnemyBullet1.png")));
            setExplosionImage(ImageIO.read(new File("data/image/LightFire.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }


        setDamageIntensity(10);

        setHeight(4);
        setWidth(21);
    }
}
