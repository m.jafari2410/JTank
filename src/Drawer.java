import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * This class draw something in screen
 * @author Mohamma Karimi
 * @version 1.0
 * @since 8.7.2018
 */

public class Drawer {

    private Drawer(){}

    /**
     * This method type a text in screen
     * @param frame frame
     * @param text text
     * @param x x position
     * @param y y position
     * @param font font of text
     * @param color color of text
     */
    public static void typeText(JFrame frame, String text, int x, int y, Font font, Color color){
        frame.add(new JPanel(){
            public void paint(Graphics g) {
                g.setColor(color);
                g.setFont(font);
                g.drawChars (text.toCharArray(), 0, text.length(), x, y);
            }
        });
    }

    /**
     * this method draw an image in frame
     * @param frame frame
     * @param imageAddress image address
     * @param x x pos
     * @param y y pos
     */
    public static void drawImage(JFrame frame, String imageAddress, int x, int y){
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(imageAddress));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        BufferedImage finalImage = image;
        frame.add(new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(finalImage, x, y, null);
            }
        });
    }

    /**
     * this method draw an image in panel
     * @param panel frame
     * @param imageAddress image address
     * @param x x pos
     * @param y y pos
     */
    public static void drawImagePanel(JPanel panel, String imageAddress, int x, int y){
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(imageAddress));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }
        BufferedImage finalImage = image;
        panel.add(new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(finalImage, x, y, null);
            }
        });
    }
}
