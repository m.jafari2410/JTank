/**
 * This class describes an stupid enemy which has a method to follow the player and do a suicide operation.
 * This class also has another method to check whether it is the time that stupid enemy should be showed or not.
 * After checking the method initStupidEnemy which returns a boolean, in condition of true the method follow player
 * is called.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 7.12.2018
 */

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

public class StupidEnemy extends Enemy {
    private boolean isInitiate = false;
    //number of updating angle of stupid enemy
    private int updateAngleNumber;
    private double angle;
    private int damageIntensity = 50;
    private Image firstImage;
    private Image secondImage;
    private Image suicideImage;
    private int imageChooser;
    private Audio audio;

    /**
     * This is constructor and calls super constructor.
     *
     * @param positionInMap is position of enemy in map.
     */
    public StupidEnemy(Coordinate positionInMap) {
        super(positionInMap);
        try {
            firstImage = ImageIO.read(new File("data/image/KhengEnemy.png"));
            secondImage = ImageIO.read(new File("data/image/KhengEnemy3.png"));
            setAfterDeathImage(ImageIO.read(new File("data/image/blood2.png")));
            suicideImage = ImageIO.read(new File("data/image/selfExplosion.png"));
            setDestroyAudio(new Audio("data/sound/killStupidEnemy.wav"));
            audio = new Audio("data/sound/stupidEnemySuicide.wav");
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        switch (Operator.levelOfGame){
            case 0:
                setHealth(50);
                break;
            case 1:
                setHealth(80);
                break;
            case 2:
                setHealth(100);
                break;
        }

        setHeight(55);
        setWidth(60);

        setShowNumberOfDeathImage(0);

        setAnchor(new Coordinate(getWidth() / 2 , getHeight() / 2));
    }

    /**
     * This method updates state of the stupid enemy.
     *
     * @param playerCoordinate position of player tank.
     */
    @Override
    public void updateState(Coordinate playerCoordinate) {
        setPlayerCoordinate(playerCoordinate);
        if (isInitiate | initStupidEnemy(playerCoordinate)) {
            followPlayer();
        }
    }

    /**
     * This method initiates enemy really. For first it checks to see whether the player tank is visible to enemy or not.
     * After that if distance of enemy from player tank was less than a certain amount then returns true.
     *
     * @return true if the enemy is seen by player tank and also if distance of enemy from player tank was less than a certain amount.
     */
    private boolean initStupidEnemy(Coordinate playerCoordination) {
        if (!isPlayerVisible())
            return false;
        int length = (int) Math.sqrt(Math.pow(playerCoordination.getX() - getPositionInScreen().getX(), 2) + Math.pow(playerCoordination.getY() - getPositionInScreen().getY(), 2));
        if (length > 600) {
            return false;
        }
        isInitiate = true;
        setDrawPermit(true);
        return true;
    }

    /**
     * This method makes stupid enemy follow player tank and do a suicide operation.
     */
    public void followPlayer() {
        int length = 6;
        while (updateAngleNumber > 10) {
            angle = getAngle();
            updateAngleNumber = 0;
        }
        updateAngleNumber++;
        Coordinate secondPos = new Coordinate((int) (getPositionInScreen().getX() + length * Math.cos(angle)), (int) (getPositionInScreen().getY() + length * Math.sin(angle)));
        if (Operator.intersects(this, secondPos) == null) {
            setPositionInMap(new Coordinate((int) (getPositionInMap().getX() + length * Math.cos(angle)), (int) (getPositionInMap().getY() + length * Math.sin(angle))));
        } else {
            if (Operator.intersects(this, secondPos) instanceof Tank) {
                Operator.hurtPlayer(damageIntensity);
                setHealth(0);
                audio.start(0);
                setAfterDeathImage(suicideImage);
                setShowNumberOfAfterDeathImage(20);
            }
        }

    }

    @Override
    public AffineTransform getAffineTransform() {
        double rotationAngle = getAngle() - 90 * Math.PI / 180;
        if (!hasUpdatePermit()) {
            rotationAngle = 0;
        }
        AffineTransform affineTransform = AffineTransform.getTranslateInstance(getPositionInScreen().getX(), getPositionInScreen().getY());
        affineTransform.rotate(rotationAngle, getAnchor().getX(), getAnchor().getY());
        return affineTransform;
    }

    @Override
    public Image getImage() {
        if (imageChooser < 15) {
            setImage(firstImage);
            imageChooser++;
        } else if (imageChooser < 30) {
            setImage(secondImage);
            imageChooser++;
        } else
            imageChooser = 0;
        return super.getImage();
    }
}
