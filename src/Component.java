import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * This class is used to model every thing that exists in the map. And all components in map extends this class.
 *
 * @author Mohammad Jafari
 * @version 3.0.0
 * @since 7.7.2018
 */
public abstract class Component {
    //image of the component
    private Image image;
    private  int height = 100;
    private  int width = 100;

    /**
     * This method sets image of component.
     * @param image image of component.
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * This method gets image of component.
     * @return image of component.
     */
    public Image getImage() {
        return image;
    }

    public  void setHeight(int height) {
        this.height = height;
    }

    public  void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
