import javax.imageio.ImageIO;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

/**
 * This class extends class SmartEnemy and has no extra method implementation yet.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 7.14.2018
 */
public class BigEnemy extends SmartEnemy {
    /**
     * This is constructor and calls super constructor.
     * @param positionInMap is position of enemy in map.
     */
    public BigEnemy(Coordinate positionInMap) {
        super(positionInMap);
        try {
            setImage(ImageIO.read(new File("data/image/BigEnemy.png")));
            setImageOfGun(ImageIO.read(new File("data/image/BigEnemyGun.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setAnchor(new Coordinate(20, 20));

        setBullet(new BigEnemyBullet());

        setLimitNumberOfRepetitiveShoots(60);

        switch (Operator.levelOfGame){
            case 0:
                setHealth(80);
                break;
            case 1:
                setHealth(130);
                break;
            case 2:
                setHealth(200);
                break;
        }
    }
}
