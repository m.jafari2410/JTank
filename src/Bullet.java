import java.awt.*;
import java.util.HashMap;

/**
 * This class models shot.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 5.7.2018
 */
public abstract class Bullet extends Component implements Cloneable {
    //position of the bullet
    private int initXPos;
    private int initYPos;
    private int xPos;
    private int yPos;
    //how much it is dangerous.
    private int damageIntensity;
    private int length;
    private double angle;
    private Image explosionImage;
    private Audio shootVoice ;
    //to see if bullet has crashed
    private boolean removed = false;

    /**
     * This method updates position of the shot.
     */
    public void updatePosition() {
        length += 8;
        xPos = (int) (length * Math.cos(angle) + initXPos);
        yPos = (int) (length * Math.sin(angle) + initYPos);
    }

    /**
     * This method gets next second position to see if crash will happened.
     * @return ext second position
     */
    public int getNextXPos() {
        return (int) ((length + 12) * Math.cos(angle) + initXPos - GameFrame.screenPosition.getX());
    }

    /**
     * This method gets next second position to see if crash will happened.
     * @return ext second position
     */
    public int getNextYPos() {
        return (int) ((length + 12) * Math.sin(angle) + initYPos - GameFrame.screenPosition.getY());
    }

    /**
     * @return x position of the shot
     */
    public int getXPos() {
        return xPos - GameFrame.screenPosition.getX();
    }

    /**
     * @return y position of the shot
     */
    public int getYPos() {
        return yPos - GameFrame.screenPosition.getY();
    }

    /**
     * @return damage intensity of the bullet.
     */
    public int getDamageIntensity() {
        return damageIntensity;
    }

    /**
     * This method increases damage intense .
     */
    public void upgrade() {
        damageIntensity += 25;
    }


    /**
     * @return angle of path of the bullet
     */
    public double getAngle() {
        return angle;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Bullet getInstance(int initXPos, int initYPos, int targetX, int targetY) throws CloneNotSupportedException {
        this.initXPos = initXPos + GameFrame.screenPosition.getX();
        this.initYPos = initYPos + GameFrame.screenPosition.getY();
        angle = Math.atan2((double) (targetY - initYPos), (double) (targetX - initXPos));
        length = 0;
        return (Bullet) clone();
    }

    /**
     * This method sets image of explosion of bullet.
     * @param explosionImage explosion image of bullet.
     */
    public void setExplosionImage(Image explosionImage) {
        this.explosionImage = explosionImage;
    }


    /**
     * This method gets image of explosion of bullet.
     * @return  explosion image of bullet.
     */
    public Image getExplosionImage() {
        return explosionImage;
    }

    public Audio getShootVoice() {
        return shootVoice;
    }

    public void setShootVoice(Audio shootVoice) {
        this.shootVoice = shootVoice;
    }

    /**
     * If bullet has removed.
     * @return true if bullet has removed.
     */
    public boolean hasRemoved() {
        return removed;
    }

    /**
     * This method sets the bullet to be removed.
     * @param removed true if bullet must be removed.
     */
    public void setRemoved(boolean removed) {
        this.removed = removed;
    }

    public void setDamageIntensity(int damageIntensity) {
        this.damageIntensity = damageIntensity;
    }
}
