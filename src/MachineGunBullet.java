import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * This class extends class bullets and models shots of the machine guns.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.7.2018
 */
public class MachineGunBullet extends Bullet {
    public MachineGunBullet() {
        setShootVoice(new Audio("data/sound/tank/machineGun.wav"));
        try {
            setImage(ImageIO.read(new File("data/image/LightBullet.png")));
            setExplosionImage(ImageIO.read(new File("data/image/LightFire.png")));
        } catch (IOException e) {
            Errors.handler("loadingError");
        }

        setDamageIntensity(20);

        setHeight(1);
        setWidth(13);
    }
}
